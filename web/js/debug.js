/*
 * debug.js
 */

const IMGSTART = 0x8000;
const IMGLEN = 0x8000;
const IMGEND = (IMGSTART+IMGLEN);
const NOBREAKPOINT = -1;

const BASECCR = 0x60;
const CBIT = 0x1;
const NOTCBIT = 0xfe;
const ZBIT = 0x2;
const NOTZBIT = 0xfd;
const NBIT = 0x4;
const NOTNBIT = 0xfb;
const IBIT = 0x8;
const NOTIBIT = 0xf7;
const HBIT = 0x10;
const NOTHBIT = 0xef;
const VBIT = 0x80;
const NOTVBIT = 0x7f;

class Processor {
  constructor (p, admod, opcodes, opcodes9E6, opcodes9ED, opcodes9EE) {
    this.prt = p;
    this.admod = admod;
    this.opcodes = opcodes;
    this.opcodes9E6 = opcodes9E6;
    this.opcodes9ED = opcodes9ED;
    this.opcodes9EE = opcodes9EE;
    this.init ();
    this.ports = null;
  }

  init () {
    this.oldPc = this.oldHx=-1;
    this.sp=0xff;
    this.pc = this.hx = 0;
    this.oldA=0;
    this.a=0xff;
    this.oldCcr=0;
    this.ccr=0x68;
    this.procRunning = false;
    this.bpAddress = NOBREAKPOINT;
    this.tmpBpAddress = NOBREAKPOINT;
    this.memory = new Uint8Array(new ArrayBuffer(IMGEND));  // Alloca la RAM-ROM-I/O
  };

  hex8(num) {
    var s = "0" + num.toString(16);
    return s.substr(s.length-2);
  }

  hex16(num) {
    var s = "000" + num.toString(16);
    return s.substr(s.length-4);
  }

  hex (string, index) {
    var s = string[index] + string[index+1];
    return parseInt(s, 16);
  }

  getAddr (modo) {
    var addr = 0;
    switch (modo) {
      case this.admod.DIR_REL :
      case this.admod.DD :         // MOV dir, dir (4e)
      case this.admod.DIR8 :
        addr = this.read(this.pc++);
        break;
      case this.admod.REL :
        addr = this.rel();
        break;
      case this.admod.INH :
        addr =  0;
        break;
      case this.admod.IMM_REL :
      case this.admod.IMM :
      case this.admod.IMD :        // MOV IMM,DIR (6e)
      case this.admod.IMM16 :
      case this.admod.DIX_P :  // MOV mem,mem - DIR/IX+ (5E)
        addr =  this.pc++;
        break;
      case this.admod.EXT :
      case this.admod.JEXT :
        addr = this.read(this.pc++) << 8;
        addr |= this.read(this.pc++);
        break;
      case this.admod.IX :
        addr = this.hx;
        break;
      case this.admod.IX1 :
        addr = this.hx + this.read(this.pc++);
        break;
      case this.admod.IX2 :
        addr = this.read(this.pc++) << 8;
        addr = this.hx + addr + this.read(this.pc++);
        break;
      case this.admod.SP1 :
      case this.admod.SP1_REL :
        addr = this.sp + this.read(this.pc++);
        break;
      case this.admod.SP2 :
        addr = this.read(this.pc++) << 8;
        addr = this.sp + addr + this.read(this.pc++);
        break;
      case this.admod.IX_P_REL : // CBEQ ,IX+,rel (71)
      case this.admod.IX_P_D :     // MOV ,IX,DIR (7e)
        addr = this.hx++;
        break;
      case this.admod.IX1_P_REL :  // CBEQ off,IX+,rel (61)
        addr = this.hx++ + this.read(this.pc++);
        break;
    }

    switch (modo) {
      case this.admod.DIR_REL :
      case this.admod.DD :         // MOV dir, dir (4e)
      case this.admod.DIR8 :
      case this.admod.IMM16 :
      case this.admod.DIX_P :  // MOV mem,mem - DIR/IX+ (5E)
      case this.admod.EXT :
      case this.admod.IX :
      case this.admod.IX1 :
      case this.admod.IX2 :
      case this.admod.SP1 :
      case this.admod.SP1_REL :
      case this.admod.SP2 :
      case this.admod.IX_P_REL : // CBEQ ,IX+,rel (71)
      case this.admod.IX_P_D :     // MOV ,IX,DIR (7e)
      case this.admod.IX1_P_REL :  // CBEQ off,IX+,rel (61)
        if (this.firstOp < 0) {
          this.firstOp = addr;
        } else {
          this.secondOp = addr;
        }
        break;
    }

    return addr & 0xffff;
  }

  signExtend8 (n) {
    if (n & 0x80) {
      n = (-1 & ~0xff) | n;
    }
    return n;
  }

  rel () {
    return this.pc + this.signExtend8(this.read(this.pc++)) + 1;
  }

  push (d) {
    this.write (this.sp--, d);
  }

  pull () {
    return this.read (++(this.sp));
  }

  noop () {
  }

  adc (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a + m + (this.ccr & 0x1);
    this.ccr = (this.ccr & 0x8) | 0x60;
    if (((this.a&0x80) != 0 && (m&0x80) != 0 && (r&0x80) == 0) || ((this.a&0x80) == 0 && (m&0x80) == 0 && (r&0x80) != 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if (((this.a&0x8) != 0 && (m&0x8) != 0) || ((m&0x8) != 0 && (r&0x8) == 0) || ((r&0x8) == 0 && (this.a&0x8) != 0)) { // H - half-carry
      this.ccr |= HBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((this.a & 0xff) != 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((r&0x100) != 0) { // C - Carry
      this.ccr |= CBIT;
    }
    this.a = r & 0xff;
  }

  add (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a + m;
    this.ccr = (this.ccr & 0x8) | 0x60;
    if (((this.a&0x80) != 0 && (m&0x80) != 0 && (r&0x80) == 0) || ((this.a&0x80) == 0 && (m&0x80) == 0 && (r&0x80) != 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if (((this.a&0x8) != 0 && (m&0x8) != 0) || ((m&0x8) != 0 && (r&0x8) == 0) || ((r&0x8) == 0 && (this.a&0x8) != 0)) { // H - half-carry
      this.ccr |= HBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((r&0x100) != 0) { // C - Carry
      this.ccr |= CBIT;
    }
    this.a = r & 0xff;
  }

  andl (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a & m;
    this.ccr = (this.ccr & 0x79) | 0x60;
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    this.a = r;
  }

  asl (m) {
    var r = (m  << 1) & 0xfe;
    this.ccr = (this.ccr & 0x78) | 0x60;
    if (((m&0x80) != 0 && (r&0x80) != 0) || ((m&0x80) == 0 && (r&0x80) == 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((m&0x80) != 0) { // C - Carry
      this.ccr |= CBIT;
    }
    return r;
  }

  asr (m) {
    var r = ((m  >> 1) & 0x7f) || (m&0x80) != 0;
    this.ccr = (ccr & 0x78) | 0x60;
    if (((m&0x1) != 0 && (r&0x80) != 0) || ((m&0x1) == 0 && (r&0x80) == 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((m&0x1) != 0) { // C - Carry
      this.ccr |= CBIT;
    }
    return r;
  }

  bitl (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a & m;
    this.ccr = (this.ccr & 0x79) | 0x60;
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
  }

  cmp (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a - m;
    this.ccr = (this.ccr & 0x78) | 0x60;
    // V: A7 & /M7 & /R7 | /A7 & M7 & R7
    if (((this.a&0x80) != 0 && (m&0x80) == 0 && (r&0x80) == 0) || ((this.a&0x80) == 0 && (m&0x80) != 0 && ((r&0x80) != 0))) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((r & 0x100) != 0) { // C - Carry
      this.ccr |= CBIT;
    }
  }

  com (m) {
    var r = ~m;
    this.ccr = (this.ccr & BASECCR) | (CBIT);
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }

    return r;
  }

  comm (am) {
    var addr = this.getAddr (am);
    this.write (addr, this.com (this.read (addr)));
  }

  cphx (am) {
    var addr = getAddr (am);
    var r,m;
    m = (this.read(addr) << 8) || this.read (addr+1);
    r = this.hx - m;
    this.ccr = (this.ccr & 0x78) | BASECCR;
    if (((this.hx&0x8000) != 0 && (m&0x8000) == 0 && (r&0x8000) == 0) || ((this.hx&0x8000) == 0 && (m&0x8000) != 0 && (r&0x8000) != 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x8000) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if (!(r & 0xffff)) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if (((this.hx&0x8000) == 0 && (m&0x8000) != 0) || ((m&0x8000) != 0 && (r&0x8000) != 0) || ((r&0x8000) != 0 & (this.hx&0x8000) == 0)) { // C - Carry
      this.ccr |= CBIT;
    }
  }

  cpx (am) {
    var addr = getAddr (am);
    var r,m;
    m = this.read(addr);
    r = (this.hx&0xff) - m;
    this.ccr = (this.ccr & 0x78) | BASECCR;
    if (((this.hx&0x80) != 0 && (m&0x80) == 0 && (r&0x80) == 0 )|| ((this.hx&0x80) == 0 && (m&0x80) != 0 && (r&0x80) != 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if (((this.hx&0x80) == 0 && (m&0x80) != 0) || ((m&0x80) != 0 && (r&0x80) != 0) || ((r&0x80) != 0 && (this.hx&0x80) == 0)) { // C - Carry
      this.ccr |= CBIT;
    }
  }

  daa () {
    var r,cf = 0;
    if ((this.ccr & CBIT) != 0) {
      if ((this.ccr&HBIT) != 0) {
        cf = 0x66;
      } else {
        if ((this.a&0xf) > 9) {
          cf = 0x66;
          this.ccr |= CBIT;
        } else {
          cf = 0x60;
          this.ccr |= CBIT;
        }
      }
    } else {
      if ((this.ccr&HBIT) != 0) {
        if (this.a > 0x90) {
          cf = 0x66;
          this.ccr |= CBIT;
        } else {
          cf = 0x6;
        }
      } else {
        if ((this.a&0xf) > 9) {
          if ((this.a & 0xf0) < 9) {
            cf = 0x6;
          } else {
            cf = 0x66;
            this.ccr |= CBIT;
          }
        } else {
          if ((this.a&0xf0) > 9) {
            cf = 0x60;
            this.ccr |= CBIT;
          }
        }
      }
    }
    r = this.a + cf;
    this.ccr = (this.ccr & 0x79) | BASECCR;
    if ((r & 0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    this.a = r;
  }

  dnz (am) {
    var addr = this.getAddr (am);
    var m = this.read(addr) -1;
    this.write (addr,m);
    return m;
  }

  dec (m) {
    var r = m - 1;
    this.ccr = (this.ccr & (BASECCR | CBIT)) | (BASECCR);
    if ((r&0x80) == 0 & (m&0x80) == 0) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }

    return r;
  }

  decm (am) {
    var addr = this.getAddr (am);
    this.write (addr, this.dec (this.read (addr)));
  }

  div () {
    var d = (this.hx&0xff00) | (this.a & 0xff);
    var x = (this.hx&0xff);
    this.ccr = (this.ccr & 0xfc) | (BASECCR);
    if (x != 0) {
      this.a = d / x;
      this.hx = ((d % x) << 8) | x;
    } else {
      this.ccr |= CBIT;
    }
    if ((this.a & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
  }

  eor (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a ^ m;
    this.ccr = (this.ccr & 0x79) | 0x60;
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    this.a = r;
  }

  inc (m) {
    var r = m + 1;
    this.ccr = (this.ccr & (BASECCR | CBIT)) | (BASECCR);
    if ((this.a & 0x80) == 0 && (r & 0x80) != 0) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r& 0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }

    return r;
  }

  incm (am) {
    var addr = this.getAddr (am);
    this.write (addr, this.inc (this.read (addr)));
  }

  jsr (am) {
    var addr = this.getAddr(am);
    this.push (this.pc & 0xff);
    this.push ((this.pc >> 8) & 0xff);
    this.pc = addr;
  }

  lda (am) {
    var r = this.read (this.getAddr (am));
    this.ccr = (this.ccr & 0x79) | 0x60;
    if ((r & 0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    this.a = r;
  }

  ldhx (am) {
    var addr = this.getAddr (am);
    var m;
    m = (this.read(addr) << 8) | this.read (addr+1);
    this.ccr = (this.ccr & 0x79) | BASECCR;
    if ((m&0x8000) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((m & 0xffff)==0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    this.hx = m;
  }

  ldx (am) {
    var r = this.read (this.getAddr (am));
    this.ccr = (this.ccr & 0x79) | 0x60;
    if (r & 0x80) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    this.hx = (this.hx & 0xff00) | (r & 0xff)
  }

  lsr (m) {
    var r = (m  >> 1) & 0x7f;
    this.ccr = (cthis.cr & 0x78) | 0x60;
    if ((m&0x1) != 0) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((m&0x1) != 0) { // C - Carry
      this.ccr |= CBIT;
    }
    return r;
  }

  lsrm (am) {
    var addr = this.getAddr (am);
    this.write (addr, this.lsr (this.read (addr)));
  }

  mul () {
    var d = (this.hx&0xff) * this.a;
    this.ccr = (this.ccr & 0xee) | (BASECCR);
    this.hx = (this.hx&0xff00) | ((d >> 8) & 0xff);
    this.a = d & 0xff;
  }

  neg (m) {
    var r = - m;
    this.ccr = (this.ccr & 0x78) | 0x60;
    if ((m&0x80) != 0 && (r&0x80) != 0) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((r & 0xff)!=0) { // C - Carry
      this.ccr |= CBIT;
    }
    return r;
  }

  negm (am) {
    var addr = this.getAddr (am);
    this.write (addr, neg (this.read (addr)));
  }

  ora (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a | m;
    this.ccr = (this.ccr & 0x79) | 0x60;
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    this.a = r;
  }

  rol (m) {
    var r = (m  << 1) & 0xfe | (this.ccr & 0x1);
    this.ccr = (this.ccr & 0x78) | 0x60;
    if (((r&0x80) == 0 && (m&0x80) == 0) || ((r&0x80) != 0 && (m&0x80) != 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((m&0x80) != 0) { // C - Carry
      this.ccr |= CBIT;
    }
    return r;
  }

  rolm (am) {
    var addr = this.getAddr (am);
    this.write (addr, this.rol (this.read (addr)));
  }


  ror (m) {
    var r = (m  >> 1) & 0x7f | ((this.ccr << 7) & 0x80);
    this.ccr = (this.ccr & 0x78) | 0x60;
    if (((m&0x1) != 0 && (r&0x80) != 0) || ((m&0x1) == 0 && ! (r&0x80) == 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if ((m&0x1) != 0) { // C - Carry
      this.ccr |= CBIT;
    }
    return r;
  }

  rorm (am) {
    var addr = this.getAddr (am);
    this.write (addr, this.ror (this.read (addr)));
  }

  sbc (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a - m - (this.ccr & 0x1);
    this.ccr = (this.ccr & 0x78) | 0x60;
    if (((this.a&0x80) != 0 && (m&0x80) == 0 && (r&0x80) == 0) || ((this.a&0x80) == 0 && (m&0x80) != 0 && (r&0x80) != 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if (((this.a&0x80) == 0 && (m&0x80) != 0) || ((m&0x80) != 0 && (r&0x80) != 0) || ((r&0x80) != 0 && (this.a&0x80) == 0)) { // C - Carry
      this.ccr |= CBIT;
    }
    this.a = r;
  }

  sta (am) {
    this.write (this.getAddr (am), this.a);
    this.ccr &= 0x79;
    if (this.a & 0x80) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((this.a & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
  }

  sthx (am) {
    var addr = this.getAddr (am);
    this.write (addr, this.hx >> 8);
    this.write (addr + 1, hx);
    this.ccr &= 0x79;
    if ((this.hx&0x8000) != 0) { // N - Negative
      ccr |= NBIT;
    }
    if (!(this.hx & 0xffff)) { // Z - Zero
      ccr |= ZBIT;
    }
  }

  stx (am) {
    this.write (this.getAddr (am), this.hx);
    this.ccr &= 0x79;
    if ((this.hx&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((hx & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
  }

  stop () {
  }

  sub (am) {
    var m = this.read (this.getAddr (am));
    var r = this.a - m;
    this.ccr = (this.ccr & 0x78) | 0x60;
    if (((this.a&0x80) != 0 && (m&0x80) == 0 && (r&0x80) == 0) || ((this.a&0x80) == 0 && (m&0x80) != 0 && (r&0x80) != 0)) { // V - overflow
      this.ccr |= VBIT;
    }
    if ((r&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if ((r & 0xff) == 0) { // Z - Zero
      this.ccr |= ZBIT;
    }
    if (((this.a&0x80) == 0 && (m&0x80) != 0) || ((m&0x80) != 0 && (r&0x80) != 0) || ((r&0x80) != 0 && (this.a&0x80) == 0)) { // C - Carry
      this.ccr |= CBIT;
    }
    this.a = r;
  }

  tst (m) {
    if ((m&0x80) != 0) { // N - Negative
      this.ccr |= NBIT;
    }
    if (!(m & 0xff)) { // Z - Zero
      this.ccr |= ZBIT;
    }
  }

  tstm (am) {
    var addr = this.getAddr (am);
    this.tst (this.read (addr));
  }

  wait () {
  }

  emulate9E () {
    var addr;
    var am;
    var opcode = this.read (this.pc++);
    switch (opcode & 0xf0) {
      case 0x60 :
        am = this.opcodes9E6[opcode & 0xf].op_mode;
        break;
      case 0xd0 :
        am = this.opcodes9ED[opcode & 0xf].op_mode;
        break;
      case 0xe0 :
        am = this.opcodes9EE[opcode & 0xf].op_mode;
        break;
    }
    switch (opcode) {
      case 0x60 : // NEG Indirizzamento SP1 Lunghezza 3
        this.negm (am);
        break;
      case 0x61 : // CBEQ Indirizzamento SP1_REL Lunghezza 4
        if (this.a == this.read(this.getAddr(am))) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x62 : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0x63 : // COM Indirizzamento SP1 Lunghezza 3
        this.comm(am);
        break;
      case 0x64 : // LSR Indirizzamento SP1 Lunghezza 3
        this.lsrm (am);
        break;
      case 0x65 : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0x66 : // ROR Indirizzamento SP1 Lunghezza 3
        this.rorm (am);
        break;
      case 0x67 : // ASR Indirizzamento SP1 Lunghezza 3
        addr = this.getAddr(am);
        this.write (addr, this.asr (this.read(addr)));
        break;
      case 0x68 : // LSL Indirizzamento SP1 Lunghezza 3
        addr = this.getAddr(am);
        this.write (addr, this.asl (this.read(addr)));
        break;
      case 0x69 : // ROL Indirizzamento SP1 Lunghezza 3
        this.rolm (am);
        break;
      case 0x6a : // DEC Indirizzamento SP1 Lunghezza 3
        this.decm (am);
        break;
      case 0x6b : // DBNZ Indirizzamento SP1_REL Lunghezza 4
        if (this.dnz(am)) {
          this.pc = this.getAddr(REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x6c : // INC Indirizzamento SP1 Lunghezza 3
        this.incm (am);
        break;
      case 0x6d : // TST Indirizzamento SP1 Lunghezza 3
        this.tstm (am);
        break;
      case 0x6e : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0x6f : // CLR Indirizzamento SP1 Lunghezza 3
        this.write( this.getAddr(am), 0);
        break;
      case 0xD0 : // SUB Indirizzamento SP2 Lunghezza 4
        this.sub (am);
        break;
      case 0xD1 : // CMP Indirizzamento SP2 Lunghezza 4
        this.cmp (am);
        break;
      case 0xD2 : // SBC Indirizzamento SP2 Lunghezza 4
        this.sbc (am);
        break;
      case 0xD3 : // CPX Indirizzamento SP2 Lunghezza 4
        this.cpx (am);
        break;
      case 0xD4 : // AND Indirizzamento SP2 Lunghezza 4
        this.andl (am);
        break;
      case 0xD5 : // BIT Indirizzamento SP2 Lunghezza 4
        this.bitl (am);
        break;
      case 0xD6 : // LDA Indirizzamento SP2 Lunghezza 4
        this.lda (am);
        break;
      case 0xD7 : // STA Indirizzamento SP2 Lunghezza 4
        this.sta (am);
        break;
      case 0xD8 : // EOR Indirizzamento SP2 Lunghezza 4
        this.eor (am);
        break;
      case 0xD9 : // ADC Indirizzamento SP2 Lunghezza 4
        this.adc (am);
        break;
      case 0xDa : // ORA Indirizzamento SP2 Lunghezza 4
        this.ora (am);
        break;
      case 0xDb : // ADD Indirizzamento SP2 Lunghezza 4
        this.add (am);
        break;
      case 0xDc : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0xDd : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0xDe : // LDX Indirizzamento SP2 Lunghezza 4
        this.ldx (am);
        break;
      case 0xDf : // STX Indirizzamento SP2 Lunghezza 4
        this.stx (am);
        break;
      case 0xE0 : // SUB Indirizzamento SP1 Lunghezza 3
        this.sub (am);
        break;
      case 0xE1 : // CMP Indirizzamento SP1 Lunghezza 3
        this.cmp (am);
        break;
      case 0xE2 : // SBC Indirizzamento SP1 Lunghezza 3
        this.sbc (am);
        break;
      case 0xE3 : // CPX Indirizzamento SP1 Lunghezza 3
        this.cpx (am);
        break;
      case 0xE4 : // AND Indirizzamento SP1 Lunghezza 3
        this.andl (am);
        break;
      case 0xE5 : // BIT Indirizzamento SP1 Lunghezza 3
        this.bitl (am);
        break;
      case 0xE6 : // LDA Indirizzamento SP1 Lunghezza 3
        this.lda (am);
        break;
      case 0xE7 : // STA Indirizzamento SP1 Lunghezza 3
        this.sta (am);
        break;
      case 0xE8 : // EOR Indirizzamento SP1 Lunghezza 3
        this.eor (am);
        break;
      case 0xE9 : // ADC Indirizzamento SP1 Lunghezza 3
        this.adc (am);
        break;
      case 0xEa : // ORA Indirizzamento SP1 Lunghezza 3
        this.ora (am);
        break;
      case 0xEb : // ADD Indirizzamento SP1 Lunghezza 3
        this.add (am);
        break;
      case 0xEc : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0xEd : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0xEe : // LDX Indirizzamento SP1 Lunghezza 3
        this.ldx (am);
        break;
      case 0xEf : // STX Indirizzamento SP1 Lunghezza 3
        this.stx (am);
        break;
      default :
        this.noop();
        break;
    }
  }

  getPc () {
    return this.pc & 0xffff;
  }

  getSp () {
    return this.sp & 0xffff;
  }

  getHx () {
    return this.hx & 0xffff;
  }

  getA () {
    return this.a & 0xff;
  }

  getCcr () {
    return this.ccr & 0xff;
  }

  getMem (i) {
    if (i < IMGEND) {
      for (var p = this.ports; p != null; p = p.getNext ()) {
        if (p.isPort (i)) {
          return p.getValue (i);
        }
      }
      return this.memory[i] & 0xff;
    }
    return 0;
  }

  setMem(i, d) {
    d &= 0xff;
    if (i < IMGEND) {
      for (var p = this.ports; p !=  null; p = p.getNext ()) {
        if (p.isPort (i)) {
          p.setValue (i, d);
        }
      }
      this.memory[i] = d;
    }
  }

  setPc (PC) {
    this.pc = PC;
  }

  setHx (HX) {
    this.hx = this.HX;
  }

  setA (A) {
    this.a = A & 0xff;
  }

  setCcr (CCR) {
    this.ccr = CCR & 0xff;
  }

  getFirstOp () {
    return this.firstOp;
  }

  getSecondOp () {
    return this.secondOp;
  }

  emulateProgram (hexfile) {
    // buffers: https://developer.mozilla.org/it/docs/Web/JavaScript/Typed_arrays
    this.prt.printf ("Carico il file %s; %d byte di memoria disponibile\n", hexfile, IMGEND);
    this.firstOp = this.secondOp = -1;
    this.readS19 (hexfile, 0, IMGEND, this.memory);
    this.reset ();
  }

  reset () {
    this.pc = ((this.memory[0xfffe] & 0xff) << 8) | (this.memory[0xffff] & 0xff);
    this.sp = 0x00ff;
  }

  emulate () {
    var opcode;
    var addr,addrd;
    var am;
    this.firstOp = this.secondOp = -1;
    opcode = this.read (this.pc++);
    am = this.opcodes[opcode].op_mode;
    switch (opcode) {
      case 0x00 : // BRSET0 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x01) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          //this.pc ++;
        }
        break;
      case 0x01 : // BRCLR0 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x01) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x02 : // BRSET1 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x02) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x03 : // BRCLR1 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x02) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x04 : // BRSET2 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x04) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x05 : // BRCLR2 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x04) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x06 : // BRSET3 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x08) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x07 : // BRCLR3 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x08) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x08 : // BRSET4 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x10) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x09 : // BRCLR4 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x10) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x0a : // BRSET5 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x20) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x0b : // BRCLR5 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x20) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x0c : // BRSET6 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x40) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x0d : // BRCLR6 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x40) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x0e : // BRSET7 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x80) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x0f : // BRCLR7 Indirizzamento DIR_REL Lunghezza 3
        if (this.read (this.getAddr(am)) & 0x80) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x10 : // BSET0 Indirizzamento DIR Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) | 0x01);
        break;
      case 0x11 : // BCLR0 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) & 0xfe);
        break;
      case 0x12 : // BSET1 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) | 0x02);
        break;
      case 0x13 : // BCLR1 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) & 0xfd);
        break;
      case 0x14 : // BSET2 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) | 0x04);
        break;
      case 0x15 : // BCLR2 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) & 0xfb);
        break;
      case 0x16 : // BSET3 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) | 0x08);
        break;
      case 0x17 : // BCLR3 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) & 0xf7);
        break;
      case 0x18 : // BSET4 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) | 0x10);
        break;
      case 0x19 : // BCLR4 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) & 0xef);
        break;
      case 0x1a : // BSET5 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) | 0x20);
        break;
      case 0x1b : // BCLR5 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) & 0xdf);
        break;
      case 0x1c : // BSET6 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) | 0x40);
        break;
      case 0x1d : // BCLR6 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) & 0xbf);
        break;
      case 0x1e : // BSET7 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) | 0x80);
        break;
      case 0x1f : // BCLR7 Indirizzamento DIR_REL Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.readMem (addr) & 0x7f);
        break;
      case 0x20 : // BRA Indirizzamento this.admod.REL Lunghezza 2
        this.pc = this.getAddr(this.admod.REL);
        break;
      case 0x21 : // BRN Indirizzamento this.admod.REL Lunghezza 2
        this.pc++;
        break;
      case 0x22 : // BHI Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x3) != 0) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x23 : // BLS Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x3) != 0) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x24 : // BCC Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x1) != 0) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x25 : // BCS Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x1) != 0) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x26 : // BNE Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x2) != 0) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x27 : // BEQ Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x2) != 0) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x28 : // BHCC Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x4) != 0) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x29 : // BHCS Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x4) != 0) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x2a : // BPL Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x4) != 0) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x2b : // BMI Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x4) != 0) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x2c : // BMC Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x8) != 0) {
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x2d : // BMS Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x8) != 0) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x2e : // BIL Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x8) != 0) {    // Testa il piedino "esterno" di "int"
          this.pc ++;
        } else {
          this.pc = this.getAddr(this.admod.REL);
        }
        break;
      case 0x2f : // BIH Indirizzamento this.admod.REL Lunghezza 2
        if ((this.ccr & 0x8) != 0) {    // Testa il piedino "esterno" di "int"
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x30 : // NEG Indirizzamento DIR8 Lunghezza 2
        negm (am);
        break;
      case 0x31 : // CBEQ Indirizzamento DIR8 Lunghezza 3
        if (this.a == this.read(this.getAddr(am))) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x32 : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0x33 : // COM Indirizzamento DIR8 Lunghezza 2
        this.comm(am);
        break;
      case 0x34 : // LSR Indirizzamento DIR8 Lunghezza 2
        this.lsrm (am);
        break;
      case 0x35 : // STHX Indirizzamento DIR8 Lunghezza 2
        this.sthx (am);
        break;
      case 0x36 : // ROR Indirizzamento DIR8 Lunghezza 2
        this.rorm (am);
        break;
      case 0x37 : // ASR Indirizzamento DIR8 Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.asr (this.read(addr)));
        break;
      case 0x38 : // LSL Indirizzamento DIR8 Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.asl (this.read(addr)));
        break;
      case 0x39 : // ROL Indirizzamento DIR8 Lunghezza 2
        this.rolm (am);
        break;
      case 0x3a : // DEC Indirizzamento DIR8 Lunghezza 2
        this.decm (am);
        break;
      case 0x3b : // DBNZ Indirizzamento DIR_REL Lunghezza 3
        if (this.dnz(am)) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x3c : // INC Indirizzamento DIR8 Lunghezza 2
        this.incm (am);
        break;
      case 0x3d : // TST Indirizzamento DIR8 Lunghezza 2
        this.tstm (am);
        break;
      case 0x3e : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0x3f : // CLR Indirizzamento DIR8 Lunghezza 2
        this.write( this.getAddr(am), 0);
        break;
      case 0x40 : // NEGA Indirizzamento INH Lunghezza 1
        this.a = this.neg(this.a);
        break;
      case 0x41 : // CBEQA Indirizzamento IMM_REL Lunghezza 3
        if (this.a == this.read(this.getAddr(am))) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x42 : // MUL Indirizzamento INH Lunghezza 1
        this.mul ();
        break;
      case 0x43 : // COMA Indirizzamento INH Lunghezza 1
        this.a = this.com(this.a);
        break;
      case 0x44 : // LSRA Indirizzamento INH Lunghezza 1
        this.a = this.lsr (this.a);
        break;
      case 0x45 : // LDHX Indirizzamento IMM16 Lunghezza 3
        this.ldhx(am);
        this.pc++;
        break;
      case 0x46 : // RORA Indirizzamento INH Lunghezza 1
        this.a = this.ror(this.a);
        break;
      case 0x47 : // ASRA Indirizzamento INH Lunghezza 1
        this.a = asr (this.a);
        break;
      case 0x48 : // LSLA Indirizzamento INH Lunghezza 1
        this.a = this.asl (this.a);
        break;
      case 0x49 : // ROLA Indirizzamento INH Lunghezza 1
        this.a = this.rol(this.a);
        break;
      case 0x4a : // DECA Indirizzamento INH Lunghezza 1
        this.a = this.dec(this.a);
        break;
      case 0x4b : // DBNZA Indirizzamento INH Lunghezza 2
        if ((--(this.a) & 0xff) != 0) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x4c : // INCA Indirizzamento INH Lunghezza 1
        this.a = this.inc(this.a);
        break;
      case 0x4d : // TSTA Indirizzamento INH Lunghezza 1
        this.tst (this.a);
        break;
      case 0x4e : // MOV Indirizzamento DD Lunghezza 3
        addr = this.getAddr(this.admod.DIR8);
        addrd = this.getAddr(this.admod.DIR8);
        this.write (addrd, this.read(addr));
        break;
      case 0x4f : // CLRA Indirizzamento INH Lunghezza 1
        this.a = 0;
        break;
      case 0x50 : // NEGX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.neg((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x51 : // CBEQX Indirizzamento IMM_REL Lunghezza 3
        if ((this.hx & 0x00ff) == this.read(this.getAddr(am))) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x52 : // DIV Indirizzamento INH Lunghezza 1
        this.div ();
        break;
      case 0x53 : // COMX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.com((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x54 : // LSRX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.lsr((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x55 : // LDHX Indirizzamento DIR8 Lunghezza 2
        this.ldhx(am);
        break;
      case 0x56 : // RORX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.ror ((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x57 : // ASRX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.asr((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x58 : // LSLX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.asl ((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x59 : // ROLX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.rol ((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x5a : // DECX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.dec((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x5b : // DBNZX Indirizzamento INH Lunghezza 2
        this.hx = ((this.hx & 0xff00) != 0) | (((this.hx & 0x00ff) - 1) & 0xff);
        if ((this.hx&0xff) != 0) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x5c : // INCX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.inc ((this.hx & 0x00ff)) & 0xff);
        break;
      case 0x5d : // TSTX Indirizzamento INH Lunghezza 1
        this.tst (this.hx);
        break;
      case 0x5e : // MOV Indirizzamento DIX_P Lunghezza 2
        addr = this.getAddr(this.admod.DIR8);
        addrd = this.getAddr(this.admod.IX_P_D);
        this.write (addrd, this.read(addr));
        break;
      case 0x5f : // CLRX Indirizzamento INH Lunghezza 1
        this.hx &= 0xff00;
        break;
      case 0x60 : // NEG Indirizzamento IX1 Lunghezza 2
        this.negm (am);
        break;
      case 0x61 : // CBEQ Indirizzamento IX1_P_REL Lunghezza 3
        if (this.a == this.read(this.getAddr(am))) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x62 : // NSA Indirizzamento INH Lunghezza 1
        this.a = ((this.a >> 4) & 0x0f) | ((this.a << 4) & 0xf0);
        break;
      case 0x63 : // COM Indirizzamento IX1 Lunghezza 2
        this.comm(am);
        break;
      case 0x64 : // LSR Indirizzamento IX1 Lunghezza 2
        this.lsrm (am);
        break;
      case 0x65 : // CPHX Indirizzamento IMM16 Lunghezza 3
        this.cphx (am);
        this.pc++;
        break;
      case 0x66 : // ROR Indirizzamento IX1 Lunghezza 2
        this.rorm (am);
        break;
      case 0x67 : // ASR Indirizzamento IX1 Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.asr (this.read(addr)));
        break;
      case 0x68 : // LSL Indirizzamento IX1 Lunghezza 2
        addr = this.getAddr(am);
        this.write (addr, this.asl (this.read(addr)));
        break;
      case 0x69 : // ROL Indirizzamento IX1 Lunghezza 2
        this.rolm (am);
        break;
      case 0x6a : // DEC Indirizzamento IX1 Lunghezza 2
        this.decm (am);
        break;
      case 0x6b : // DBNZ Indirizzamento IX1 Lunghezza 3
        if (this.dnz(am)) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x6c : // INC Indirizzamento IX1 Lunghezza 2
        this.incm (am);
        break;
      case 0x6d : // TST Indirizzamento IX1 Lunghezza 2
        this.tstm (am);
        break;
      case 0x6e : // MOV Indirizzamento IMD Lunghezza 3
        addr = this.getAddr(this.admod.IMM);
        addrd = this.getAddr(this.admod.DIR8);
        this.write (addrd, this.read(addr));
        break;
      case 0x6f : // CLR Indirizzamento IX1 Lunghezza 2
        this.write( this.getAddr(am), 0);
        break;
      case 0x70 : // NEG Indirizzamento IX Lunghezza 1
        this.negm (am);
        break;
      case 0x71 : // CBEQ Indirizzamento IX_P_REL Lunghezza 2
        if (this.a == this.read(this.getAddr(am))) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x72 : // DAA Indirizzamento INH Lunghezza 1
        this.daa();
        break;
      case 0x73 : // COM Indirizzamento IX Lunghezza 1
        this.comm(am);
        break;
      case 0x74 : // LSR Indirizzamento IX Lunghezza 1
        this.lsrm (am);
        break;
      case 0x75 : // CPHX Indirizzamento DIR8 Lunghezza 2
        this.cphx (am);
        break;
      case 0x76 : // ROR Indirizzamento IX Lunghezza 1
        this.rorm (am);
        break;
      case 0x77 : // ASR Indirizzamento IX Lunghezza 1
        var addr = this.getAddr(am);
        this.write (addr, this.asr (this.read(addr)));
        break;
      case 0x78 : // LSL Indirizzamento IX Lunghezza 1
        addr = this.getAddr(am);
        this.write (addr, this.asl (this.read(addr)));
        break;
      case 0x79 : // ROL Indirizzamento IX Lunghezza 1
        this.rolm (am);
        break;
      case 0x7a : // DEC Indirizzamento IX Lunghezza 1
        this.decm (am);
        break;
      case 0x7b : // DBNZ Indirizzamento IX Lunghezza 2
        if (dnz(am)) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x7c : // INC Indirizzamento IX Lunghezza 1
        this.incm (am);
        break;
      case 0x7d : // TST Indirizzamento IX Lunghezza 1
        this.tstm (am);
        break;
      case 0x7e : // MOV Indirizzamento IX_P_D Lunghezza 2
        addr = this.getAddr(IX_P_D);
        addrd = this.getAddr(DIR8);
        this.write (addrd, this.read(addr));
        break;
      case 0x7f : // CLR Indirizzamento IX Lunghezza 1
        this.write( this.getAddr(am), 0);
        break;
      case 0x80 : // RTI Indirizzamento INH Lunghezza 1
        this.ccr = this.pull();
        this.a = this.pull();
        this.hx = (this.hx & 0xff00) | (this.pull () & 0xff);
        this.pc = (this.pull () << 8) & 0xff00;
        this.pc |= this.pull() & 0xff;
        break;
      case 0x81 : // RTS Indirizzamento INH Lunghezza 1
        this.pc = (this.pull () << 8) & 0xff00;
        this.pc |= this.pull() & 0xff;
        break;
      case 0x82 : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0x83 : // SWI Indirizzamento INH Lunghezza 1
        this.ccr |= IBIT;
        this.push (this.pc);
        this.push (this.pc >> 8);
        this.push (this.hx);
        this.push (this.a);
        this.push (this.ccr);
        this.pc = ((this.read (0xfffc) << 8) | 0xff00) | (this.read (0xfffd) & 0xff);
        break;
      case 0x84 : // TAP Indirizzamento INH Lunghezza 1
        this.ccr = this.a | BASECCR;
        break;
      case 0x85 : // TPA Indirizzamento INH Lunghezzaa = this.ccr;
        this.noop ();
        break;
      case 0x86 : // PULA Indirizzamento INH Lunghezza 1
        this.a = this.pull();
        break;
      case 0x87 : // PSHA Indirizzamento INH Lunghezza 1
        this.push(this.a);
        break;
      case 0x88 : // PULX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.pull() & 0xff);
        break;
      case 0x89 : // PSHX Indirizzamento INH Lunghezza 1
        push ((this.hx & 0x00ff));
        break;
      case 0x8a : // PULH Indirizzamento INH Lunghezza 1
        this.hx = ((this.pull() << 8) & 0xff00) | (this.hx & 0x00ff);
        break;
      case 0x8b : // PSHH Indirizzamento INH Lunghezza 1
        push ((this.hx & 0xff00) >> 8);
        this.break;
      case 0x8c : // CLRH Indirizzamento INH Lunghezza 1
        this.hx &= 0x00ff;
        break;
      case 0x8d : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0x8e : // STOP Indirizzamento INH Lunghezza 1
        this.stop ();
        break;
      case 0x8f : // WAIT Indirizzamento INH Lunghezza 1
        this.wait ();
        break;
      case 0x90 : // BGE Indirizzamento this.admod.REL Lunghezza 2
        // If (N ^ V) = 0 ( N==1&&V==1 || N==0&&V==0)
        if ((this.ccr & 0x84) == 0 || (this.ccr & 0x84) == 0x84) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x91 : // BLT Indirizzamento this.admod.REL Lunghezza 2
        // If (N ^ V) = 1
        if ((this.ccr & 0x84) == 0x80 || (this.ccr & 0x84) == 0x04) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x92 : // BGT Indirizzamento this.admod.REL Lunghezza 2
        // If (Z) | (N ^ V) = 0
        // if ((this.ccr & 0x2) != 0 || !(this.ccr & 0x84) || (this.ccr & 0x84) == 0x84) {
        if ((this.ccr & 0x2) == 0 && ((this.ccr & 0x84) == 0 || (this.ccr & 0x84) == 0x84)) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x93 : // BLE Indirizzamento this.admod.REL Lunghezza 2
        // If (Z) | (N ^ V) = 1
        if ((this.ccr & 0x2) != 0 || (this.ccr & 0x84) == 0x04 || (this.ccr & 0x84) == 0x80) {
          this.pc = this.getAddr(this.admod.REL);
        } else {
          this.pc ++;
        }
        break;
      case 0x94 : // TXS Indirizzamento INH Lunghezza 1
        this.sp = this.hx - 1;
        break;
      case 0x95 : // TSX Indirizzamento INH Lunghezza 1
        this.hx = this.sp + 1;
        break;
      case 0x96 : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0x97 : // TAX Indirizzamento INH Lunghezza 1
        this.hx = (this.hx & 0xff00) | (this.a & 0xff);
        break;
      case 0x98 : // CLC Indirizzamento INH Lunghezza 1
        this.ccr &= NOTCBIT;
        break;
      case 0x99 : // SEC Indirizzamento INH Lunghezza 1
        this.ccr |= CBIT;
        break;
      case 0x9a : // CLI Indirizzamento INH Lunghezza 1
        this.ccr &= NOTIBIT;
        break;
      case 0x9b : // SEI Indirizzamento INH Lunghezza 1
        this.ccr |= IBIT;
        break;
      case 0x9c : // RSP Indirizzamento INH Lunghezza 1
        this.sp |= 0xff;
        break;
      case 0x9d : // NOP Indirizzamento INH Lunghezza 1
        this.noop();
        break;
      case 0x9e :
        this.emulate9E ();
        break;
      case 0x9f : // TXA Indirizzamento INH Lunghezza 1
        this.a = this.hx;
        break;
      case 0xa0 : // SUB Indirizzamento IMM Lunghezza 2
        this.sub (am);
        break;
      case 0xa1 : // CMP Indirizzamento IMM Lunghezza 2
        this.cmp (am);
        break;
      case 0xa2 : // SBC Indirizzamento IMM Lunghezza 2
        this.sbc (am);
        break;
      case 0xa3 : // CPX Indirizzamento IMM Lunghezza 2
        this.cpx (am);
        break;
      case 0xa4 : // AND Indirizzamento IMM Lunghezza 2
        this.andl (am);
        break;
      case 0xa5 : // BIT Indirizzamento IMM Lunghezza 2
        this.bitl (am);
        break;
      case 0xa6 : // LDA Indirizzamento IMM Lunghezza 2
        this.lda (am);
        break;
      case 0xa7 : // AIS Indirizzamento IMM Lunghezza 2
        this.sp += this.signExtend8(this.read(this.getAddr(am)));
        break;
      case 0xa8 : // EOR Indirizzamento IMM Lunghezza 2
        this.eor (am);
        break;
      case 0xa9 : // ADC Indirizzamento IMM Lunghezza 2
        this.adc (am);
        break;
      case 0xaa : // ORA Indirizzamento IMM Lunghezza 2
        this.ora (am);
        break;
      case 0xab : // ADD Indirizzamento IMM Lunghezza 2
        this.add (am);
        break;
      case 0xac : //  Indirizzamento INH Lunghezza 0
        this.noop();
        break;
      case 0xad : // BSR Indirizzamento this.admod.REL Lunghezza 2
        addr = this.getAddr(this.admod.REL);
        this.push (this.pc & 0xff);
        this.push ((this.pc >> 8) & 0xff);
        this.pc = addr;
        break;
      case 0xae : // LDX Indirizzamento IMM Lunghezza 2
        this.ldx (am);
        break;
      case 0xaf : // AIX Indirizzamento IMM Lunghezza 2
        this.hx += this.signExtend8(this.read(this.getAddr(am)));
        break;
      case 0xb0 : // SUB Indirizzamento DIR8 Lunghezza 2
        this.sub (am);
        break;
      case 0xb1 : // CMP Indirizzamento DIR8 Lunghezza 2
        this.cmp (am);
        break;
      case 0xb2 : // SBC Indirizzamento DIR8 Lunghezza 2
        this.sbc (am);
        break;
      case 0xb3 : // CPX Indirizzamento DIR8 Lunghezza 2
        this.cpx (am);
        break;
      case 0xb4 : // AND Indirizzamento DIR8 Lunghezza 2
        this.andl (am);
        break;
      case 0xb5 : // BIT Indirizzamento DIR8 Lunghezza 2
        this.bitl (am);
        break;
      case 0xb6 : // LDA Indirizzamento DIR8 Lunghezza 2
        this.lda (am);
        break;
      case 0xb7 : // STA Indirizzamento DIR8 Lunghezza 2
        this.sta (am);
        break;
      case 0xb8 : // EOR Indirizzamento DIR8 Lunghezza 2
        this.eor (am);
        break;
      case 0xb9 : // ADC Indirizzamento DIR8 Lunghezza 2
        this.adc (am);
        break;
      case 0xba : // ORA Indirizzamento DIR8 Lunghezza 2
        this.ora (am);
        break;
      case 0xbb : // ADD Indirizzamento DIR8 Lunghezza 2
        this.add (am);
        break;
      case 0xbc : // JMP Indirizzamento DIR8 Lunghezza 2
        this.pc = this.getAddr(am);
        break;
      case 0xbd : // JSR Indirizzamento DIR8 Lunghezza 2
        this.jsr(am);
        break;
      case 0xbe : // LDX Indirizzamento DIR8 Lunghezza 2
        this.ldx (am);
        break;
      case 0xbf : // STX Indirizzamento DIR8 Lunghezza 2
        this.stx (am);
        break;
      case 0xc0 : // SUB Indirizzamento EXT Lunghezza 3
        this.sub (am);
        break;
      case 0xc1 : // CMP Indirizzamento EXT Lunghezza 3
        this.cmp (am);
        break;
      case 0xc2 : // SBC Indirizzamento EXT Lunghezza 3
        this.sbc (am);
        break;
      case 0xc3 : // CPX Indirizzamento EXT Lunghezza 3
        this.cpx (am);
        break;
      case 0xc4 : // AND Indirizzamento EXT Lunghezza 3
        this.andl (am);
        break;
      case 0xc5 : // BIT Indirizzamento EXT Lunghezza 3
        this.bitl (am);
        break;
      case 0xc6 : // LDA Indirizzamento EXT Lunghezza 3
        this.lda (am);
        break;
      case 0xc7 : // STA Indirizzamento EXT Lunghezza 3
        this.sta (am);
        break;
      case 0xc8 : // EOR Indirizzamento EXT Lunghezza 3
        this.eor (am);
        break;
      case 0xc9 : // ADC Indirizzamento EXT Lunghezza 3
        this.adc (am);
        break;
      case 0xca : // ORA Indirizzamento EXT Lunghezza 3
        this.ora (am);
        break;
      case 0xcb : // ADD Indirizzamento EXT Lunghezza 3
        this.add (am);
        break;
      case 0xcc : // JMP Indirizzamento JEXT Lunghezza 3
        this.pc = this.getAddr(am);
        break;
      case 0xcd : // JSR Indirizzamento JEXT Lunghezza 3
        this.jsr(am);
        break;
      case 0xce : // LDX Indirizzamento EXT Lunghezza 3
        this.ldx (am);
        break;
      case 0xcf : // STX Indirizzamento EXT Lunghezza 3
        this.stx (am);
        this.break;
      case 0xd0 : // SUB Indirizzamento IX2 Lunghezza 3
        this.sub (am);
        break;
      case 0xd1 : // CMP Indirizzamento IX2 Lunghezza 3
        this.cmp (am);
        break;
      case 0xd2 : // SBC Indirizzamento IX2 Lunghezza 3
        this.sbc (am);
        break;
      case 0xd3 : // CPX Indirizzamento IX2 Lunghezza 3
        this.cpx (am);
        break;
      case 0xd4 : // AND Indirizzamento IX2 Lunghezza 3
        this.andl (am);
        break;
      case 0xd5 : // BIT Indirizzamento IX2 Lunghezza 3
        this.bitl (am);
        break;
      case 0xd6 : // LDA Indirizzamento IX2 Lunghezza 3
        this.lda (am);
        break;
      case 0xd7 : // STA Indirizzamento IX2 Lunghezza 3
        this.sta (am);
        break;
      case 0xd8 : // EOR Indirizzamento IX2 Lunghezza 3
        this.eor (am);
        break;
      case 0xd9 : // ADC Indirizzamento IX2 Lunghezza 3
        this.adc (am);
        break;
      case 0xda : // ORA Indirizzamento IX2 Lunghezza 3
        this.ora (am);
        break;
      case 0xdb : // ADD Indirizzamento IX2 Lunghezza 3
        this.add (am);
        break;
      case 0xdc : // JMP Indirizzamento IX2 Lunghezza 3
        this.pc = this.getAddr(am);
        break;
      case 0xdd : // JSR Indirizzamento IX2 Lunghezza 3
        this.jsr(am);
        break;
      case 0xde : // LDX Indirizzamento IX2 Lunghezza 3
        this.ldx (am);
        break;
      case 0xdf : // STX Indirizzamento IX2 Lunghezza 3
        this.stx (am);
        break;
      case 0xe0 : // SUB Indirizzamento IX1 Lunghezza 2
        this.sub (am);
        break;
      case 0xe1 : // CMP Indirizzamento IX1 Lunghezza 2
        this.cmp (am);
        break;
      case 0xe2 : // SBC Indirizzamento IX1 Lunghezza 2
        this.sbc (am);
        break;
      case 0xe3 : // CPX Indirizzamento IX1 Lunghezza 2
        this.cpx (am);
        break;
      case 0xe4 : // AND Indirizzamento IX1 Lunghezza 2
        this.andl (am);
        break;
      case 0xe5 : // BIT Indirizzamento IX1 Lunghezza 2
        this.bitl (am);
        break;
      case 0xe6 : // LDA Indirizzamento IX1 Lunghezza 2
        this.lda (am);
        break;
      case 0xe7 : // STA Indirizzamento IX1 Lunghezza 2
        this.sta (am);
        break;
      case 0xe8 : // EOR Indirizzamento IX1 Lunghezza 2
        this.eor (am);
        break;
      case 0xe9 : // ADC Indirizzamento IX1 Lunghezza 2
        this.adc (am);
        break;
      case 0xea : // ORA Indirizzamento IX1 Lunghezza 2
        this.ora (am);
        break;
      case 0xeb : // ADD Indirizzamento IX1 Lunghezza 2
        this.add (am);
        break;
      case 0xec : // JMP Indirizzamento IX1 Lunghezza 2
        this.pc = this.getAddr(am);
        break;
      case 0xed : // JSR Indirizzamento IX1 Lunghezza 2
        this.jsr(am);
        break;
      case 0xee : // LDX Indirizzamento IX1 Lunghezza 2
        this.ldx (am);
        break;
      case 0xef : // STX Indirizzamento IX1 Lunghezza 2
        this.stx (am);
        break;
      case 0xf0 : // SUB Indirizzamento IX Lunghezza 1
        this.sub (am);
        break;
      case 0xf1 : // CMP Indirizzamento IX Lunghezza 1
        this.cmp (am);
        break;
      case 0xf2 : // SBC Indirizzamento IX Lunghezza 1
        this.sbc (am);
        break;
      case 0xf3 : // CPX Indirizzamento IX Lunghezza 1
        this.cpx (am);
        break;
      case 0xf4 : // AND Indirizzamento IX Lunghezza 1
        this.andl (am);
        break;
      case 0xf5 : // BIT Indirizzamento IX Lunghezza 1
        this.bitl (am);
        break;
      case 0xf6 : // LDA Indirizzamento IX Lunghezza 1
        this.lda (am);
        break;
      case 0xf7 : // STA Indirizzamento IX Lunghezza 1
        this.sta (am);
        break;
      case 0xf8 : // EOR Indirizzamento IX Lunghezza 1
        this.eor (am);
        break;
      case 0xf9 : // ADC Indirizzamento IX Lunghezza 1
        this.adc (am);
        break;
      case 0xfa : // ORA Indirizzamento IX Lunghezza 1
        this.ora (am);
        break;
      case 0xfb : // ADD Indirizzamento IX Lunghezza 1
        this.add (am);
        break;
      case 0xfc : // JMP Indirizzamento IX Lunghezza 1
        this.pc = this.getAddr(am);
        break;
      case 0xfd : // JSR Indirizzamento IX Lunghezza 1
        this.jsr(am);
        break;
      case 0xfe : // LDX Indirizzamento IX Lunghezza 1
        this.ldx (am);
        break;
      case 0xff : // STX Indirizzamento IX Lunghezza 1
        this.stx (am);
        break;
    }
  }

  read (Addr) {
    return this.getMem (Addr);
  }

  readMem (Addr) {           // Sostituisce quella di hc08monitor, per gestire emulazione
    return (Addr < IMGEND) ? this.memory[Addr] : 0;
  }

  write (Addr, Dato) {
    if (Addr < IMGEND) {
      this.setMem(Addr, Dato);
    }
  }

  addPort (p) {
    p.setNext (this.ports);
    this.ports = p;
  }

  // A differenza del C++, ritorna un affay di 2 elementi: [0]: la lunghezza del''istruzione; [1]: l'istruzione disassemblata
  disassembly (disassPC) {
    var curop = null;
    var opcode, opcode2;
    var oprAddr = disassPC;
    var opr1,opr2;
    var params = "";

    opcode = this.read (disassPC);
    oprAddr ++;
    if (opcode == 0x9e) {
      opcode2 = this.read (disassPC+1);
      oprAddr ++;
      switch (opcode2 & 0xF0) {
        case 0x60 :
          curop = this.opcodes9E6[opcode2 & 0xf];
          break;
        case 0xD0 :
          curop = this.opcodes9ED[opcode2 & 0xf];
          break;
        case 0xE0 :
          curop = this.opcodes9EE[opcode2 & 0xf];
          break;
        default :
          return [ disassPC+2, "istruzione 9E " + opcode2.toString(16) + " illegale"];
      }
      if (curop.op_len == 0) {
        return [ disassPC+2, "istruzione 9E " + opcode2.toString(16) + " illegale"];
      }
    } else {
      curop = this.opcodes[opcode];
    }
    if (curop === undefined || curop === null || curop.op_len == 0) {
      return [ disassPC+1, "istruzione " + opcode.toString(16) + " illegale"];
    }
    switch (curop.op_mode) {
      case this.admod.DIR_REL :  // Direct8, Relative
        opr1 = this.read (oprAddr++);
        opr2 = this.signExtend8 (this.read (oprAddr++));
        opr2 += oprAddr;
        params = "$" + this.hex8(opr1) + "," + this.hex16(opr2);
        break;
      case this.admod.IMM_REL :  // Direct8, Relative
        opr1 = this.read (oprAddr++);
        opr2 = this.signExtend8 (this.read (oprAddr++));
        opr2 += oprAddr;
        params = "#" + this.hex8(opr1) + "," + this.hex16(opr2);
        break;
      case this.admod.DIR8 : // Direct8
        opr1 = this.read (oprAddr++);
        params = "$" + this.hex8(opr1);
        break;
      case this.admod.REL :  // Relative
        opr1 = this.signExtend8 (this.read (oprAddr++));
        opr1 += oprAddr;
        params = this.hex16(opr1);
        break;
      case this.admod.INH :  // Inherent
        break;
      case this.admod.IMM :  // Immediate (8 bit)
        opr1 = this.read (oprAddr++);
        params = "#" + this.hex8(opr1);
        break;
      case this.admod.IMM16 :  // Immediate (16 bit)
        opr1 = this.read (oprAddr++);
        opr2 = this.read (oprAddr++);
        params = "$" + this.hex8(opr1) + this.hex8(opr2);
        break;
      case this.admod.EXT :  // Extended: Direct/16bit addr
        opr1 = this.read (oprAddr++);
        opr2 = this.read (oprAddr++);
        params = "#" + this.hex8(opr1) + this.hex8(opr2);
        break;
      case this.admod.JEXT : // Extended: Direct/16bit addr - Jump
        opr1 = this.read (oprAddr++);
        opr2 = this.read (oprAddr++);
        params = this.hex8(opr1) + this.hex8(opr2);
        break;
      case this.admod.IX :
        params = ",X";
        break;
      case this.admod.IX1 :  // opr8,X X plus offset
        opr1 = this.signExtend8 (this.read (oprAddr++));
        params = opr1.toString() + ",X";
        break;
      case this.admod.IX2 :  // Opr16, X
        opr1 = this.read (oprAddr++);
        opr2 = this.read (oprAddr++);
        params = "$" + this.hex8(opr1) + this.hex8(opr2) + ",X";
        break;
      case this.admod.SP1 :  // opr8,SP
        opr1 = this.read (oprAddr++);
        params = "$" + this.hex8(opr1) + ", X+";
        break;
      case this.admod.SP1_REL :  // opr8,SP, rel
        opr1 = this.read (oprAddr++);
        opr2 = this.signExtend8 (this.read (oprAddr++));
        opr2 += oprAddr;
        params = "$" + this.hex8(opr1) + ", SP, " + this.hex16(opr2);
        break;
      case this.admod.SP2 :  // opr16,SP
        opr1 = this.read (oprAddr++);
        opr2 = this.read (oprAddr++);
        params = "$" + this.hex8(opr1) + this.hex8(opr2) + ", SP";
        break;
      case this.admod.DIX_P :  // Direct, X+
        opr1 = this.read (oprAddr++);
        params = "$" + this.hex8(opr1) + ", X+";
        break;
      case this.admod.IX1_P_REL :
        opr1 = this.signExtend8 (this.read (oprAddr++));
        opr2 = this.signExtend8 (this.read (oprAddr++));
        opr2 += oprAddr;
        params = "$" + this.hex8(opr1) + ", X+, " + this.hex16(opr2);
        break;
      case this.admod.IX_P_REL :
        opr1 = this.signExtend8 (this.read (oprAddr++));
        opr1 += oprAddr;
        params = ", X+, " + this.hex16(opr1);
        break;
      case this.admod.IX_P_D : // (this.hx & 0x00ff)+, $dir8
        opr1 = this.read (oprAddr++);
        params = ", X+, $" + this.hex8(opr1);
        break;
      case this.admod.DD :
        break;
      case this.admod.IMD :  // Immediate 8 bit, Direct 8 bit
        opr1 = this.read (oprAddr++);
        opr2 = this.read (oprAddr++);
        params = "#" + this.hex8(opr1) + ",$" + this.hex8(opr2);
        break;
    }
    return [disassPC + curop.op_len, curop.op_name + " " + params];
  }

  readS19 (line, start, imglen, prog) {
    var len,i,cks, address;
    var hex;
    var j;

    line = [...line]; // Spezza la linea in un array di caratteri.

    for (i = 0;i < imglen; i++) {
      prog [i] = 0xff;
    }

    for (j = 0; j < line.length; ) {
      // puts (line);
      if (line[j] != 's' && line [j] != 'S') {
        this.prt.printf ("WARNING: Record non riconosciuto\n");
      } else {
        len = this.hex (line, j + 2);
        for (cks = i = 0; i<len; i++) {
          cks += this.hex (line,  2 * i + j + 2);  // Chacksum dalla lunghezza compresa al checksum escluso
        }

        cks = (cks & 0xff) ^ 0xff;
        if (cks != this.hex (line, 2 * i + j + 2)) {
          this.prt.printf ("ERRORE: Linea scorretta (Lunghezza %d (%02x), Checksum calcolato %02x, letto %02x)\n", len, len, cks,
            this.hex (line, 2 * i + j + 2));
          break;
        }

        switch (line[j + 1]) {
          case '0' :  // E' una stringa di descrizione
            // this.prt.printf ("Record S0 di descrizione; contentuo:\n");
            for (i = 0; i<len - 3; i++) {
              this.prt.printf (String.fromCharCode(this.hex(line, 2 * i + j + 8)));
            }
            this.prt.printf ("\n\n");
            break;
          case '1' :  // Mi interessano solo i dati con indirizzo this.a 16 bit
            // this.prt.printf ("Record S1 di dati con indirizzo this.a 16 bit; contentuo");
            address = this.hex (line, j + 4) * 256 + this.hex (line, j + 6);
            for (i = 0; i < len - 3; i++) {
              hex = this.hex (line, 2 * i + j + 8);
              if (address >= start && address < (start + imglen)) {
                prog [address - start] = hex;
              }
              /* if (i % 16 == 0) {
                this.prt.printf ("\n%04x ", address);
              }
              this.prt.printf (" %02x", hex); */
              address ++;
            }
            // this.prt.printf ("\n");
            break;
          case '9' :  // Terminatore
            this.prt.printf ("\nRecord S9: Fine dati HEX\n");
            break;
          default:
            this.prt.printf ("\nWARNING: Record s%c non supportato\n", line [j + 1]);
            break;
        }
        // Termina la linea
        for (;line[j] != '\n' && line[j] != '\r' && j < line.length; j++);
        for (;(line[j] == '\n' || line[j] == '\r') && j < line.length; j++);
      }
    }
  }

  //const unsigned char *pgmCode () { return procConfigs [curProcessor].pgm; };
  //int pgmCodeStart () { return procConfigs [curProcessor].pgmStart; };
  //int pgmCodeLen () { return procConfigs [curProcessor].pgmLen; };
  //const unsigned char *eraCode () { return procConfigs [curProcessor].era; };
  //int eraCodeStart () { return procConfigs [curProcessor].eraStart; };
  //int eraCodeLen () { return procConfigs [curProcessor].eraLen; };
  //int eraAddress (int clean) { return (clean) ?
    //procConfigs [curProcessor].staddressC : procConfigs [curProcessor].staddressP; };
  //int pageLen () { return procConfigs [curProcessor].pagelen; };

  //void setupCom (const char *comname) { Com::setupCom (comname); init (); };
  //void stackprep ();
  //void readRegisters ();
  //char *step (int Bp);
  //bool runEnded ();
  //void endStep ();
  //void go (int start);
  //int resetVector (unsigned char *vector);

  //int nextBp (int mode, int opcode, int opcode2, op *curop);

  //char disass [20]; // Disassembly buffer

  //static unsigned char clean [];

  //static progRoutines procConfigs [];
  //static int prrtAddrs [];  // Indrizzi da verificare per il fingerprint
  //static unsigned char prints [][NUMFPPOINTS];
  //int curProcessor;   // Processore corrente (riconosciuto)

  //bool procRunning;

  //int fingerPrint (void);

  //int programS19 (const char *hexfile);
  //void uploadPrg (const unsigned char *pgm, int start, int num); /* private */
  //void sendWrCommand (int stad, int datalen, unsigned char *data, int pagelen); /* private */
  //int isErased (unsigned char *resVector);
  //void programChip (const char *hexfile);
  //int verifyChip (const char *hexfile);
  //void clearChip ();
  //bool isRunning () { return procRunning; };
  //bool hasBp () { return bpAddress != NOBREAKPOINT; };
  //bool hasBp (int refBp) { return bpAddress != NOBREAKPOINT && bpAddress != refBp; };
  //void setBp (int where) { bpAddress = where; };
  //void setTempBp (int where) { tmpBpAddress = where; };

  //export class Monitor /* : public Com */ {
    //int getInt (void);
    //// Funzioni "hardware"
    //void PowerOnReset (void);
    //void StandardReset (void);
    //// Funzioni per eseguire le chiamate del monitor
    //int iread (void);
    //void iwrite (int Dato);
    //int readsp (void);
    //void run (void);
}
