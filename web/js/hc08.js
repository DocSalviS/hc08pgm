/*
 * hc08.js
 * Possibili miglioramenti:
 * Consentire di dare un file hex (o asm) per parametro (GET) (priprio il contenuto):
 * https://stackoverflow.com/questions/831030/how-to-get-get-request-parameters-in-javascript
 * https://davidwalsh.name/query-string-javascript
 * Impossibile con POST.
 */


//// hexFile contiene i dati del file /home/salvi/Desktop/4Iin/20190925-max/ese.s19
//// bisognerà poi caricarlo da disco (drag & drop o file dialog).
var hexFile = "S10B008115080C1B6C01600260\nS118EE00AE81F6B7805CF6B1809302B7805C51890220F320FEE5\nS105FFFEEE000F\nS9030000FC\n";
var hexName = "ese.s19";

var prt = null;
var dbg = null;
var fullScr = null;
var msg = null;

function onLoad() {
  msg = document.getElementById("msgPanel");
  prt = new Printer();
  dbg = new Processor (prt, admod, opcodes, opcodes9E6, opcodes9ED, opcodes9EE);
  fullScr = new FullScreen (dbg);
  fullScr.run(hexFile);  // non ha aperto la seriale, quindi non la chiude
}

function message(m) {
  msg.innerHTML += m + "<br>";
  msg.scrollIntoView(false);
}

function handleFileSelect(evt) {
  evt.stopPropagation();
  evt.preventDefault();

  var files = evt.dataTransfer.files; // FileList object.

  // files is a FileList of File objects. List some properties.
  loadFile(files[0]);
}

function assemble(name, hexFile) {
  var ajax = new XMLHttpRequest();
  var formData = new FormData();
  var screen=fullScr;
  formData.append('name', name);
  formData.append('content', hexFile);
  ajax.open('POST', 'assemble.php', true);
  ajax.onreadystatechange = function() {
    if (this.readyState == 4) {
      if (this.status == 200) {
        try {
          var r = JSON.parse(this.responseText);
          for (var i = 0; i < r.messages.length; i++) {
            message(r.messages[i]);
          }
          if (r.errore == 0) {
            screen.run(r.hexfile);
          }
        } catch(e) {
          message("<span class=\"error\">ERRORE: " + e.message + "</span><br>" + this.responseText);
        }
      } else {
        message("<span class=\"error\">ERRORE HTML: " + this.status + "<br>" + this.responseText + "</span>");
      }
    }
  };
  message('<span class=\"message\">Caricato un sorgente assembler, Lo invio all&apos;assemblatore</span>');
  ajax.send(formData);
}

function loadFile(f) {
  clearMsg();
  message('Carico <strong>' + escape(f.name) + '</strong> ' + f.size.toString() + ' bytes');
  var re = /(?:\.([^.]+))?$/;
  var ext = re.exec(f.name)[1];
  if (ext == "s19" || ext == "S19") {
    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        hexFile = e.target.result;
        fullScr.run(hexFile);
      };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsText(f);
  } else if(ext == "asm") {
    if(window.location.protocol == 'file:') {
      message('La pagina &egrave; eseguita in locale, quindi non posso richiamare l&apos;assemblatore');
    } else {
      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          hexFile = e.target.result;
          assemble(f.name, hexFile);
        };
      })(f);

      reader.readAsText(f);

    }
  } else {
    message('File di tipo non utilizzabile. Ignoro');
  }
}

function handleDragOver(evt) {
  evt.stopPropagation();
  evt.preventDefault();
  evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
}

function saveS19(hexFile, hexName) {
  var a = document.getElementById("s19");
  var file = new Blob([hexFile], {type: 'text/plain'});
  a.href = URL.createObjectURL(file);
  a.download = hexName;
  a.click();
}

function getFileDialog() {
  var inputFile = document.getElementById("file-input");
  inputFile.value="";
  inputFile.click();
}

function handleSelect(e) {
  var file = e.target.files[0];
  if (!file) {
    return;
  }
  loadFile(file);
}

var runTimer = null;

function runProgram() {
  if (runTimer == null) {
    runTimer = setInterval(function () { fullScr.step(); } , 100);
    document.getElementById("buttonRun").value="Stop";
  } else {
    clearInterval(runTimer);
    runTimer = null;
    document.getElementById("buttonRun").value="Run";
  }
}

function clearMsg() {
  msg.innerHTML="";
}
