/*
 * fullscreen.js
 */

const colors = {
  NORMAL: "colorNormal",
  RED: "colorRed",
  REDLED: "colorRedLed",
  YELLOWLED: "colorYellowLed",
  GREENLED: "colorGrenLed",
  DISPLAY: "colorDisplay"
};

class HcWindow {
  constructor (windowClass, dbg) {
    this.dbg = dbg;
    this.win = document.getElementById(windowClass);
    // this.fullRefresh(); // verrebbe eseguita prima della configurazione della finestra figlia
  }

  //keyManage () {
    //return mvwgetch (win,0,0);
  //}

  refresh () {
  }

  fullRefresh() {
    this.refresh ();
  }
}


class codeWin extends HcWindow {
  constructor (windowClass, dbg) {
    super (windowClass, dbg);
    this.start = dbg.getPc();
    this.h = 16;
    this.rowstarts = new Array();  // Alloca l'array degli inizi riga
    this.refresh ();
  }

  refresh () {
    var i, j, curr, pc;
    var content = "";
    pc = this.dbg.getPc();
    if (pc < this.start) {
      this.start = pc;
    } else {
      if (pc > this.rowstarts [this.h - 1]) {
        if (pc < this.rowstarts [this.h - 1] + (this.rowstarts [this.h - 1] - this.rowstarts [0]) / 3) {
          this.start = this.rowstarts [this.h / 2];
        } else {
          this.start = pc;
        }
      }
    }
    curr = this.start;
    for (i = 0; i < this.h; i++) {
      var r = this.dbg.disassembly (curr);
      var next = r[0];
      this.rowstarts [i] = curr;
      if (pc >= curr && pc < next) {
        content += "<span class=\"colorRed\">";
      }
      content += this.dbg.hex16(curr) + " ";
      for (j = 0; j < 4; j++) {
        content += ((curr + j < next) ? this.dbg.hex8(this.dbg.read(curr + j) & 0xff) : "  ") + " ";
      }
      content += r[1];
      if (pc >= curr && pc < next) {
        content += "</span>";
      }
      curr = next;
      content += "\n";
    }
    this.win.innerHTML = content;
  }
}

const ACCHEX = 0;
const ACCDEC = 1;
const HX = 2;
const SP = 3;
const PC = 4;
const FLAG_V = 5;
const FLAG_6 = 6;
const FLAG_5 = 7;
const FLAG_H = 8;
const FLAG_I = 9;
const FLAG_N = 10;
const FLAG_X = 11;
const FLAG_C = 12;

class registerWin extends HcWindow {
  constructor (windowClass, dbg) {
    super(windowClass, dbg);
    this.oldA = this.dbg.getA ();
    this.oldCcr = this.dbg.getCcr ();
    this.oldHx = this.dbg.getHx ();
    this.oldSp = this.dbg.getSp ();
    this.registers = [document.getElementById("reg_accHex"),
      document.getElementById("reg_accDec"),
      document.getElementById("reg_hx"),
      document.getElementById("reg_sp"),
      document.getElementById("reg_pc"),
      document.getElementById("reg_flag_v"),
      document.getElementById("reg_flag_6"),
      document.getElementById("reg_flag_5"),
      document.getElementById("reg_flag_h"),
      document.getElementById("reg_flag_i"),
      document.getElementById("reg_flag_n"),
      document.getElementById("reg_flag_x"),
      document.getElementById("reg_flag_c")];
    this.refresh ();
  }

  refresh () {
    var a = this.dbg.getA (), ccr = this.dbg.getCcr ();
    var hx = this.dbg.getHx (), sp = this.dbg.getSp ();
    var pc = this.dbg.getPc ();
    for (var i = 0; i < this.registers.length; i++) {
      this.registers[i].className="";
    }

    if (a != this.oldA) {
      this.registers[ACCHEX].className= "colorRed";
      this.registers[ACCDEC].className= "colorRed";
      this.registers[ACCHEX].innerHTML=this.dbg.hex8(a);
      this.registers[ACCDEC].innerHTML=a.toString();
      this.oldA = a;
    }
    if (hx != this.oldHx) {
      this.registers[HX].className= "colorRed";
      this.registers[HX].innerHTML=this.dbg.hex16(hx);
      this.oldHx = hx;
    }
    if (sp != this.oldSp) {
      this.registers[SP].className= "colorRed";
      this.registers[SP].innerHTML=this.dbg.hex16(sp);
      this.oldSp = sp;
    }
    this.registers[PC].innerHTML=this.dbg.hex16(pc);

    var ccrDiff = ccr ^ this.oldCcr;
    if (ccrDiff & 0x80) {
      this.registers[FLAG_V].className= "colorRed";
      this.registers[FLAG_V].innerHTML=(ccr & 0x80) ? "V" : "v";
    }

    if (ccrDiff & 0x40) {
      this.registers[FLAG_6].className= "colorRed";
      this.registers[FLAG_6].innerHTML=(ccr & 0x40) ? "1" : "0";
    }

    if (ccrDiff & 0x20) {
      this.registers[FLAG_5].className= "colorRed";
      this.registers[FLAG_5].innerHTML=(ccr & 0x20) ? "1" : "0";
    }

    if (ccrDiff & 0x10) {
      this.registers[FLAG_H].className= "colorRed";
      this.registers[FLAG_H].innerHTML=(ccr & 0x10) ? "H" : "h";
    }

    if (ccrDiff & 0x08) {
      this.registers[FLAG_I].className= "colorRed";
      this.registers[FLAG_I].innerHTML=(ccr & 0x08) ? "I" : "i";
    }

    if (ccrDiff & 0x04) {
      this.registers[FLAG_N].className= "colorRed";
      this.registers[FLAG_N].innerHTML=(ccr & 0x04) ? "N" : "n";
    }

    if (ccrDiff & 0x02) {
      this.registers[FLAG_X].className= "colorRed";
      this.registers[FLAG_X].innerHTML=(ccr & 0x02) ? "Z" : "x";
    }

    if (ccrDiff & 0x01) {
      this.registers[FLAG_C].className= "colorRed";
      this.registers[FLAG_C].innerHTML=(ccr & 0x01) ? "C" : "c";
    }

    this.oldCcr = ccr;
  }
}

const LINELEN = 8;
const LINMASK = 0xfff8;


class memoryWin extends HcWindow {
  constructor (windowClass, dbg) {
    super (windowClass, dbg);
    this.start = 0x80;
    this.h = 11;
    this.refresh ();
  }

  refresh () {
    var i,j;
    var firstOp = this.dbg.getFirstOp();
    var secondOp = this.dbg.getSecondOp();

    if (firstOp >= 0 && firstOp < this.start) {
      this.start = firstOp;
      if (secondOp >= 0 && secondOp < this.start && secondOp >= firstOp - LINELEN * this.h) {
        this.start = secondOp;
      }
    } else {
      if (firstOp >= 0 && firstOp >= this.start + LINELEN * this.h) {
        this.start = firstOp - LINELEN * (this.h - 1);
        if (secondOp >= 0 && secondOp > firstOp && secondOp < firstOp + LINELEN * (this.h - 1)) {
          this.start = secondOp - LINELEN * (this.h - 1);
        }
      }
    }

    var content = "";

    this.start &= LINMASK;  // Allinea l'inizio della memoria
    for (i = 0; i < this.h; i++) {
      content += this.dbg.hex16(this.start + LINELEN*i) + " ";
      for (j = 0; j < LINELEN; j++) {
        var addr = this.start + LINELEN*i + j;
        if (firstOp == addr || secondOp  == addr) {
         content += "<span class=\"colorRed\">";
        }
        content += this.dbg.hex8(this.dbg.getMem(addr)) + " ";
        if (firstOp == addr || secondOp  == addr) {
          content += "</span>";
        }
      }
      content += " ";
      for (j = 0; j < LINELEN; j++) {
        var c = this.dbg.getMem(this.start + LINELEN*i + j) & 0xff;
        if (firstOp == (this.start + LINELEN*i + j) || secondOp  == (this.start + LINELEN*i + j)) {
         content += "<span class=\"colorRed\">";
        }
        content += (c >= 0x20 /* ' ' */ && c < 0x7f) ? String.fromCharCode(c) : ".";
        if (firstOp == (this.start + LINELEN*i + j) || secondOp  == (this.start + LINELEN*i + j)) {
          content += "</span>";
        }
      }
      content += "\n";
    }
    this.win.innerHTML=content;
  }
};

class commandWin extends HcWindow {
  constructor (windowClass, dbg) {
    super(windowClass, dbg);
    this.refresh ();
  }

  refresh () {
  }
}

const DDROFFSET = 4;
const PUEOFFSET = 0xB;
const PTA = 0;
const PTB = 1;
const DDRA = (PTA + DDROFFSET);
const DDRB = (PTB + DDROFFSET);
const PTAPUE = (PTA + PUEOFFSET);
const PTBPUE = (PTB + PUEOFFSET);

// AGGIUNGERE OPENCOLLECTOR
class InputConnector {
  constructor (ttp, oc) {
    this.totempoles = ttp;
    this.opencollectors = oc;
    this.next = null;
    this.value = 0;
    this.input = document.getElementById("i_o_button");
  }

  setNext(nx) {
    this.next = nx;
  }

  getNext() {
    return this.next;
  }

  add(input) {
    /* Log::printf("InputConnector::add(%02x) - ttp %02x/oc %02x/Valore %02x - Ritorno %02x",
      input, totempoles, opencollectors, value,
      ((input & ~totempoles) | (value & totempoles)) & (value | ~opencollectors)); */
    this.value = (this.input.checked) ? 0 : this.opencollectors;
    return (((input & ~this.totempoles) | (this.value & this.totempoles)) & (this.value | ~this.opencollectors));
  }

  setValue(v) {
    this.value = (this.input.checked) ? 0 : this.opencollectors;
    this.value = v;
  }

  getValue() {
    return this.value;
  }
}

class OutputConnector {
  constructor (msk) {
    this.mask = msk;
    this.next = null;
    this.value = 0;
  }

  setNext(nx) {
    this.next = nx;
  }

  getNext() {
    return this.next;
  }

  out(v) {
    /* Log::printf("OutputConnector - out %x", v); */
    this.value = v;
  }
}

class IoPort {
  constructor (addr) {
    this.address = addr;
    this.next = null;
    this.value = 0xff;
  }

  isPort(rAddress) {
    return rAddress==this.address;
  }

  getValue(rAddress) {
    return this.value;
  }

  getRowValue(rAddress) {
    return this.value;
  }

  setValue(rAddress, dato) {
    this.value = dato;
  }

  resetValue(v) {
    this.value=v;
  }

  setNext (n) {
    this.next = n;
  }

  getNext () {
    return this.next;
  }
}

class GpIoPort extends IoPort {
  constructor (addr) {
    super(addr);
    this.ddr = 0;
    this.pue = 0;
    this.inputs = null;
    this.outputs = null;
  }

  evalOutput() {
    var v;
    v = this.value & this.ddr | (~this.ddr & this.pue);
    for (var curIn = this.inputs; curIn; curIn = curIn.getNext()) {
      v = curIn.add(v);
    }
    return v;
  }

  sentToOutput() {
    var v = this.evalOutput();
    var out;
    // Log::printf("GpIoPort::sendToOutput %04x %02x (%02x - %02x -> %02x)", address, value, ddr, pue, (value & ddr) | pue);
    for (out = this.outputs; out; out = out.getNext()) {
      out.out(v);
    }
  }

  addInput(input) {
    input.setNext(this.inputs);
    this.inputs = input;
  }

  addOutput (output) {
    output.setNext(this.outputs);
    this.outputs = output;
  }

  isPort(rAddress) {
    return rAddress==this.address || rAddress==this.address + DDROFFSET || rAddress==this.address + PUEOFFSET;
  }

  getValue(rAddress) {
    switch (rAddress - this.address) {
      case 0 :
        return this.evalOutput();
      case DDROFFSET:
        return this.ddr;
      case PUEOFFSET:
        return this.pue;
      default:
        return 0xff;
    }
  }

  getRowValue(rAddress) {
    switch (rAddress - this.address) {
      case 0 :
        return this.value;
      case DDROFFSET:
        return this.ddr;
      case PUEOFFSET:
        return this.pue;
      default:
        return 0xff;
    }
  }

  getVal() {
    return this.value;
  }

  getDDR() {
    return this.ddr;
  }

  getPUE() {
    return this.pue;
  }

  setValue(rAddress, dato) {
    switch (rAddress - this.address) {
      case 0 :
        this.value = dato;
        this.sentToOutput();
        break;
      case DDROFFSET:
        this.ddr = dato;
        this.sentToOutput();
        break;
      case PUEOFFSET:
        this.pue = dato;
        break;
    }
  }
}

const DISPLAYROWS = 2;
const DISPLAYCHARS = 16;
const DDRAM_LEN = 80;
const SECONDLINE = (DDRAM_LEN/2);
const SECONDADDR = SECONDLINE;

class DisplayLCD {
  constructor () {
    var r;
    this.messaggio = "";
    this.ddram = new Int8Array(new ArrayBuffer(DDRAM_LEN));;
    this.en = false;
    this.rs = false;
    this.bus = 0;
    this.data = 0;
    for (r = 0; r < DDRAM_LEN; r++) {
        this.ddram[r] = ' ';
    }

    this.status = document.getElementById("i_o_display_status");
    this.line0 = document.getElementById("i_o_display0");
    this.line1 = document.getElementById("i_o_display1");
    this.i_o_message = document.getElementById("i_o_message");

    // Reset State
    this.ddaddres = 0;
    this.dshift = 0;
    this.comm4bit = false;
    this.loByte = false;
    this.displayOn = false;
    this.twoLines = false;
  }

  refresh(win) {
    var i;
    var line0 = "";
    var line1 = "";
    for (i = 0; i < 16; i++) {
      line0 += (this.displayOn) ? String.fromCharCode(this. ddram[(this.dshift + i) % DDRAM_LEN]) : ' ';
      line1 += (this.displayOn && this.twoLines) ? String.fromCharCode(this.ddram[(this.dshift + i + SECONDLINE) % DDRAM_LEN]) : ' ';
    }
    this.line0.innetHTML = line0;
    this.line1.innetHTML = line1;
    // Visualizza lo stato dell'interfaccia
    // mvwprintw (win, 3, 21, "%02x %02x %s %s %c %c", bus, data, (en) ? "EN" : "en", (rs) ? "RS" : "rs",
    //  (comm4bit) ? '4' : '8', (comm4bit) ? ( (loByte) ? 'L' : 'H') : 'F');
    // Visualizza lo stato del display
    this.status.innetHTML = (((this.displayOn) ? 'A' : 's') + " " + this.ddaddres.toString() + " " + this.dshift.toString())

    this.i_o_message.innetHTML = this.messaggio;
    this.messaggio = "";
  }

  setEn(value) {
    if (this.en != 0 && value == 0) { // Fronte di discesa
      if (this.comm4bit) {
        // Log::printf("Reset EN: Bus %02x Dato %02x byte=%s", bus, value, (loByte) ? "lo" : "hi");
        if (this.loByte) {
          this.data = (this.data & 0xf0) | (this.bus >> 4 & 0x0f);
          this.loByte = false;
        } else {
          this.data = this.bus;
          this.loByte = true;
          this.en = value;
          return; // Non ha ricevuto il dato intero.
        }
      } else {
        // Log::printf("Reset EN: Bus %02x Dato %02x 8bit", bus, value);
        this.data = this.bus;
        this.loByte=false;
      }
      if (this.rs) {
        this.gotData();
      } else {
        this.gotCommand ();
      }
    }
    if (this.en == 0 && value != 0) {
      // Log::printf("Set EN");
    }
    this.en = value;
  }

  setRs(value) {
    this.messaggio = (value) ? "Set RS  " : "Reset RS";
    //if (value && !this.rs) {
      //Log::printf("Set RS  ");
    //}
    //if (!value && this.rs) {
      //Log::printf("Reset RS");
    //}
    this.rs = value;
  }

  setBus(value) {
    //  Log:printf("setBus: %02x", value);
    this.bus = value;
  }

  gotCommand () {
    // Log::printf("gotCommand: %02x", data);
    if (this.data == 1) { // Clear display
      this.ddaddres = 0;
      for (var i = 0; i < DDRAM_LEN; i++) {
        this.ddram[i] = ' ';
      }
    } else if ((this.data & 0xfe) == 0x02) { // Return home
      // Log::printf("Return home");
      this.ddaddres = 0;
      this.dshift = 0;
    } else if ((this.data & 0xfc) == 0x04) { // Entry mode set
      // ToDo
    } else if ((this.data & 0xf8) == 0x08) { // Display on/off control
      // Log::printf("Display on/off control - %02x", data & 0x04);
      this.displayOn = this.data & 0x04;
    } else if ((this.data & 0xf0) == 0x10) { // Cursor or display shift
      // ToDo
    } else if ((this.data & 0xe0) == 0x20) { // Function set
      this.comm4bit = !(this.data & 0x10);
      this.twoLines = this.data & 0x08;
      //Log::printf("Function set - %02x", data);
    } else if ((this.data & 0xc0) == 0x40) { // Set CGRAM address
      // Unimplemented
    } else if ((this.data & 0x80) == 0x80) { // Set DDRAM address
      this.data &= 0x7f;
      if (this.twoLines) {
        if (this.data < SECONDADDR) {
          this.ddaddres = this.data % SECONDLINE;
        } else {
          this.ddaddres = this.data - SECONDADDR + SECONDLINE;
          if (this.ddaddres > (DDRAM_LEN - DISPLAYCHARS) && this.ddaddres < DDRAM_LEN) {
            this.ddaddres = this.data - (DDRAM_LEN - DISPLAYCHARS) + SECONDLINE;
          } else if (this.ddaddres >= (SECONDLINE + DISPLAYCHARS)) {
            this.ddaddres = SECONDLINE;
          }
        }
      } else {
        this.ddaddres = this.data % DDRAM_LEN;
      }
      // Log::printf("*** Set DDRAM address: %02x (%dd) -> %02x (%dd)", data, data, ddaddres, ddaddres);
    }
  }

  gotData() {
    // Log::printf("gotData: %02x (%02x)", data, ddaddres);
    this.ddram[this.ddaddres++] = this.data;
    this.ddaddres %= DDRAM_LEN;
  }
}

const S5PIN = 0x80;
const YELLOWLED = 0x10;
const GREENLED = 0x40;
const BUSLEDS = 0x0f;

// Indirizzi dei registri
const CONFIG2 = 0x1e;
const CONFIG1 = 0x1f;

class PTALEDout extends OutputConnector {
  constructor(myPort) {
    super(YELLOWLED)
    this.port = myPort;
    this.yellowLed = document.getElementById("I_O_LEDY");
  }

  refresh (win) {
    this.yellowLed.src = ((this.port.ddr & YELLOWLED) != 0) ? (((this.value & YELLOWLED) != 0) ? "img/LedYon.png" : "img/LedYoff.png") : "img/LedYgrey.png";
  }
}

class PTBLEDout extends OutputConnector {
  constructor (myIn, myPort) {
    super (GREENLED | BUSLEDS)
    this.in = myIn;
    this.port = myPort;
    this.greenled = document.getElementById("I_O_LEDG");
    this.redLeds = [
      document.getElementById("I_O_LEDR7"),
      document.getElementById("I_O_LEDR6"),
      document.getElementById("I_O_LEDR5"),
      document.getElementById("I_O_LEDR4"),
      document.getElementById("I_O_LEDR3"),
      document.getElementById("I_O_LEDR2"),
      document.getElementById("I_O_LEDR1"),
      document.getElementById("I_O_LEDR0"),
    ];
  }

  refresh (win) {
    var i;
    var ledMask;
    var v = this.value;
    for (i = 4, ledMask = 0x8; i < 8; i++, ledMask /= 2) {
      this.redLeds[i].src = ((this.port.ddr & ledMask) != 0) ? (((v & ledMask) != 0) ? "img/LedRon.png" : "img/LedRoff.png") : "img/LedRgrey.png";
    }

    this.greenled.src = ((this.port.ddr & GREENLED) != 0) ? (((v & GREENLED) != 0) ? "img/LedGon.png" : "img/LedGoff.png") : "img/LedGgrey.png";

    if ((this.in.input.enabled && (this.port.ddr & S5PIN) != 0) || (!this.in.input.enabled && (this.port.ddr & S5PIN) == 0)) {
      this.in.input.enabled = (this.port.ddr & S5PIN) == 0;
    }
  }
}

class PTALCDout extends OutputConnector {
  constructor (disp) {
    super(YELLOWLED);
    this.display = disp;
  }

  out(v) {
    /* Log::printf("PTALCDout::OutputConnector - out %x", v & YELLOWLED); */
    this.display.setRs(v & YELLOWLED);
    this.value = v;
  }
}


class PTBLCDout extends OutputConnector {
  constructor (disp) {
    super(GREENLED | BUSLEDS);
    this.display = disp;
  }

  out(v) {
    /* Log::printf("PTBLCDout::OutputConnector - out %x", v & (GREENLED | BUSLEDS)); */
    this.display.setBus((v << 4) & 0xf0);
    this.display.setEn(v & GREENLED);
    this.value = v;
  }
}

class IoWin extends HcWindow {
  constructor (windowClass, dbg) {
    super (windowClass, dbg);
    var ptb = new GpIoPort (PTB);
    this.ptbKey = new InputConnector(0, S5PIN, ptb);
    this.ptbLED = new PTBLEDout(this.ptbKey, ptb);
    var pta = new GpIoPort (PTA);
    this.display = new DisplayLCD(pta);
    this.ptaLED = new PTALEDout(pta);
    pta.addOutput (this.ptaLED);
    pta.addOutput (new PTALCDout(this.display));
    //  PTAport(display);
    ptb.addOutput (this.ptbLED);
    ptb.addOutput (new PTBLCDout(this.display));
    ptb.addInput (this.ptbKey);
    // PTBport(display);
    this.dbg.addPort (pta);
    this.dbg.addPort (ptb);
    this.dbg.addPort (new IoPort(CONFIG2));
    this.dbg.addPort (new IoPort(CONFIG1));
    this.refresh ();
  }

  refresh () {
    this.ptaLED.refresh(this.win);
    this.ptbLED.refresh(this.win);
    this.display.refresh(this.win);
    // wrefresh (win);
  }

  KeyToggle() {
    // Log::printf("IoWin::KeyToggle(): old %02x mask %02x new %02x", ptbKey->getValue (), S5PIN, ptbKey->getValue () ^ S5PIN);
    this.ptbKey.setValue (this.ptbKey.getValue () ^ S5PIN);
    this.ptbLED.refresh(this.win);
    // wrefresh (win);
  }
}

class FullScreen {
  constructor (dbg) {
    this.dbg = dbg;
    this.code = new codeWin ("codeWindow", this.dbg);
    this.registers = new registerWin ("registerWindow", this.dbg);
    this.data = new memoryWin ("dataWindow", this.dbg);
    this.command = new commandWin ("commandWindow", this.dbg);
    this.io = new IoWin ("i_oWindow", this.dbg);
  }

  fulrefresh () {
    this.code.refresh ();
    this.registers.refresh ();
    this.data.refresh ();
    this.command.refresh ();
    this.io.refresh();
  }

  step () {
    this.dbg.emulate();
    this.fulrefresh ();
  }

  reset () {
    this.dbg.reset();
    this.fulrefresh ();
  }

  displayRefresh() {
    this.code.fullRefresh ();
    this.registers.fullRefresh ();
    this.data.fullRefresh ();
    this.command.fullRefresh ();
    this.io.fullRefresh();
  }

  run (hexFile) {
    this.dbg.emulateProgram (hexFile);
    this.displayRefresh();

    //while (true) {
      //switch (command -> keyManage ()) {
        //case 's' :  // Step
          //step ();
          //break;
        //case 'r' :  // Reset
          //reset ();
          //break;
      //case 't' :  // Commuta tasto
      //io->KeyToggle();
      //break;
      //case 'f' :  // Rinfresca il contenuto del video
        //displayRefresh();
      //break;
        //case 'q' :  // Quit
          //endwin ();
          //return;
      //}
    //}
  }
}
