/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * displaylcd.h
 * Copyright (C) 2016 salvi <stefano@salvi.mn.it>
 *
 * hc08pgm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * hc08pgm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DISPLAYLCD_H
#define DISPLAYLCD_H

#include <string.h>
#include <log.h>

#ifndef CURSES_H
struct WINDOW;
#endif

#define DISPLAYROWS		2
#define DISPLAYCHARS	16
#define DDRAM_LEN		80
#define SECONDLINE		(DDRAM_LEN/2)
// #define SECONDADDR		0x40
#define SECONDADDR		SECONDLINE

class DisplayLCD
{
public:
	DisplayLCD();
	void refresh(WINDOW * win);
	void setEn(bool value);
	void setRs(bool value) { strcpy (messaggio,(value) ? "Set RS  " : "Reset RS"); 
		if (value && !rs) {
			Log::printf("Set RS  ");
		}
		if (!value && rs) {
			Log::printf("Reset RS");
		}
		rs = value;  };
	void setBus(unsigned char value);

protected:

private:
    char ddram[DDRAM_LEN];
	int ddaddres;
	int dshift;
	bool comm4bit;  // 4 bit communication
    bool twoLines;  // Display a due linee
	bool displayOn; // Display acceso (visdualizza ddram)

	char messaggio[80];
	 
	bool loByte;	// if it has to receive lo byte
	 
	// interfaccia
	unsigned char bus;
	unsigned char data;
	bool en;
	bool rs;

	void gotCommand ();
	void gotData();
};

#endif // DISPLAYLCD_H

