/* hc08monitor.c
 * Rotunes di base per chiamare le funzione del monitor dell'HC08
 */

#include <stdio.h>
#include <stdlib.h>
#include "hc08monitor.h"

/* ************************************************************ */
/* Funzioni elementari per trasmissione/ricezione               */
/* ************************************************************ */

int Monitor::getInt (void)
{
int l,h;
  h = schGetc(STDTIMEOUT);
  l = schGetc(STDTIMEOUT);
  return ((h << 8) & 0xff00) | (l & 0xff);
}

/* ************************************************************ */
/* Funzioni che implementano i comandi veri e propri            */
/* ************************************************************ */

int Monitor::read (int Addr)
{
  schPutc (0x4a);
  schPutc ((Addr >> 8) & 0xff);
  schPutc (Addr & 0xff);
  return schGetc (STDTIMEOUT);
}

int Monitor::iread (void)
{
  schPutc (0x1a);
  return getInt ();
}

void Monitor::write (int Addr, int Dato)
{
  schPutc (0x49);
  schPutc ((Addr >> 8) & 0xff);
  schPutc (Addr & 0xff);
  schPutc (Dato & 0xff);
  mysleep (11460);  /* corrispondono a 11 bit a 9600 baud */
}

void Monitor::iwrite (int Dato)
{
  schPutc (0x19);
  schPutc (Dato & 0xff);
  mysleep (11460);  /* corrispondono a 11 bit a 9600 baud */
}

int Monitor::readsp (void)
{
int r;
  schPutc (0x0c);
  return getInt ();
}

void Monitor::run (void)
{
  schPutc (0x28);
}


/*
 * Il RESET del micro e' collegato all'RTS, invertito (rtsOn resetta, rtsOff libera)
 * Il Power della scheda e' collegato all'RTS, non invertito (dtrOn accende la scheda, dtrOff spegne la scheda)
 */

void Monitor::PowerOnReset (void)
{
  rtsOn ();           // Reset scheda nuova - pin 7, RTS
  dtrOff ();          // Tolgo alimentazione -  pin 4, DTR
  mysleep (400000);   // con 500mS l'alimentazione fa in tempo a scendere
  dtrOn ();           // Alimento scheda - pin 4, DTR
  mysleep (300000);   // Per un sicuro Power On occorrono 400mS
//  dtrOn ();           // Alimenro scheda - pin 4, DTR
  rtsOff ();           // Reset scheda nuova - pin 7, RTS
  mysleep (200000);   // 300000);   // Per un sicuro Power On occorrono 400mS

  flushInput ();
}

void Monitor::StandardReset (void)
{
  dtrOn ();           // Alimento scheda (per sicurezza) - pin 4, DTR
  rtsOn ();           // Reset scheda nuova - pin 7, RTS
  // L'impulso di reset deve durare 50 u 125 nS -> do 1 uS
  mysleep (1);
  rtsOff ();          // Reset scheda nuova - pin 7, RTS
  // Dopo l'impulso devono passare 256 cicli di Bus (quanto durano?)
  mysleep (500000);   // Sicuramente 200 mS bastano...

  flushInput ();
}

