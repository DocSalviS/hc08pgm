/* hc08monitor.h
 * prototipi delle funzioni del monitor hc08
 */

#ifndef _hc08monitor_h
#define _hc08monitor_h

#include "com.h"

class Monitor : public Com {
	int getInt (void);
public:
	Monitor (Printer *p) : Com (p) {};
	// Funzioni "hardware"
	void PowerOnReset (void);
	void StandardReset (void);
	// Funzioni per eseguire le chiamate del monitor
	int read (int Addr);
	void write (int Addr, int Dato);
	int iread (void);
	void iwrite (int Dato);
	int readsp (void);
	void run (void);
};

#endif
