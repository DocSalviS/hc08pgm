/*
 * opcodes.js
 */
// enum admod
const admod = { DIR_REL:1, IMM_REL:2, DIR8:3, REL: 4, INH: 5, IMM: 6, IMM16: 7,
    EXT: 8, JEXT: 9, IX: 10, IX1: 11, IX2: 12, SP1: 12, SP1_REL: 13, SP2: 14,
    DIX_P: 15, IX_P_REL: 16, IX1_P_REL: 17, IX_P_D: 18, DD: 19, IMD: 20 };

class op {
  constructor(op_name, op_len, op_mode, cycles) {
    this.op_name = op_name;
    this.op_len =  op_len;
    this.op_mode =  op_mode;
    this.cycles =  cycles;
  }
}

var opcodes = new Array();
/* 0 */   opcodes.push(new op("BRSET0", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRCLR0", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRSET1", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRCLR1", 3, admod.DIR_REL, 5));
      opcodes.push(new op("BRSET2", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRCLR2", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRSET3", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRCLR3", 3, admod.DIR_REL, 5));
      opcodes.push(new op("BRSET4", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRCLR4", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRSET5", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRCLR5", 3, admod.DIR_REL, 5));
      opcodes.push(new op("BRSET6", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRCLR6", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRSET7", 3, admod.DIR_REL, 5)); opcodes.push(new op("BRCLR7", 3, admod.DIR_REL, 5));
/* 1 */   opcodes.push(new op("BSET0", 2, admod.DIR8, 4)); opcodes.push(new op("BCLR0", 2, admod.DIR8, 4)); opcodes.push(new op("BSET1", 2, admod.DIR8, 4)); opcodes.push(new op("BCLR1", 2, admod.DIR8, 4));
      opcodes.push(new op("BSET2", 2, admod.DIR8, 4)); opcodes.push(new op("BCLR2", 2, admod.DIR8, 4)); opcodes.push(new op("BSET3", 2, admod.DIR8, 4)); opcodes.push(new op("BCLR3", 2, admod.DIR8, 4));
      opcodes.push(new op("BSET4", 2, admod.DIR8, 4)); opcodes.push(new op("BCLR4", 2, admod.DIR8, 4)); opcodes.push(new op("BSET5", 2, admod.DIR8, 4)); opcodes.push(new op("BCLR5", 2, admod.DIR8, 4));
      opcodes.push(new op("BSET6", 2, admod.DIR8, 4)); opcodes.push(new op("BCLR6", 2, admod.DIR8, 4)); opcodes.push(new op("BSET7", 2, admod.DIR8, 4)); opcodes.push(new op("BCLR7", 2, admod.DIR8, 4));
/* 2 */   opcodes.push(new op("BRA", 2, admod.REL, 3)); opcodes.push(new op("BRN", 2, admod.REL, 3)); opcodes.push(new op("BHI", 2, admod.REL, 3)); opcodes.push(new op("BLS", 2, admod.REL, 3));
      opcodes.push(new op("BCC", 2, admod.REL, 3)); opcodes.push(new op("BCS", 2, admod.REL, 3)); opcodes.push(new op("BNE", 2, admod.REL, 3)); opcodes.push(new op("BEQ", 2, admod.REL, 3));
      opcodes.push(new op("BHCC", 2, admod.REL, 3)); opcodes.push(new op("BHCS", 2, admod.REL, 3)); opcodes.push(new op("BPL", 2, admod.REL, 3)); opcodes.push(new op("BMI", 2, admod.REL, 3));
      opcodes.push(new op("BMC", 2, admod.REL, 3)); opcodes.push(new op("BMS", 2, admod.REL, 3)); opcodes.push(new op("BIL", 2, admod.REL, 3)); opcodes.push(new op("BIH", 2, admod.REL, 3));
/* 3 */   opcodes.push(new op("NEG", 2, admod.DIR8, 4)); opcodes.push(new op("CBEQ", 3, admod.DIR8, 5)); opcodes.push(new op("", 1, admod.INH, 1)); opcodes.push(new op("COM", 2, admod.DIR8, 4));
      opcodes.push(new op("LSR", 2, admod.DIR8, 4)); opcodes.push(new op("STHX", 2, admod.DIR8, 4)); opcodes.push(new op("ROR", 2, admod.DIR8, 4)); opcodes.push(new op("ASR", 2, admod.DIR8, 4));
      opcodes.push(new op("LSL", 2, admod.DIR8, 4)); opcodes.push(new op("ROL", 2, admod.DIR8, 4)); opcodes.push(new op("DEC", 2, admod.DIR8, 4)); opcodes.push(new op("DBNZ", 3, admod.DIR_REL, 5));
      opcodes.push(new op("INC", 2, admod.DIR8, 4)); opcodes.push(new op("TST", 2, admod.DIR8, 3)); opcodes.push(new op("", 1, admod.INH, 1)); opcodes.push(new op("CLR", 2, admod.DIR8, 3));
/* 4 */   opcodes.push(new op("NEGA", 1, admod.INH, 1)); opcodes.push(new op("CBEQA", 3, admod.IMM_REL, 4)); opcodes.push(new op("MUL", 1, admod.INH, 5)); opcodes.push(new op("COMA", 1, admod.INH, 1));
      opcodes.push(new op("LSRA", 1, admod.INH, 1)); opcodes.push(new op("LDHX", 3, admod.IMM16, 3)); opcodes.push(new op("RORA", 1, admod.INH, 1)); opcodes.push(new op("ASRA", 1, admod.INH, 1));
      opcodes.push(new op("LSLA", 1, admod.INH, 1)); opcodes.push(new op("ROLA", 1, admod.INH, 1)); opcodes.push(new op("DECA", 1, admod.INH, 1)); opcodes.push(new op("DBNZA", 2, admod.REL, 3));
      opcodes.push(new op("INCA", 1, admod.INH, 1)); opcodes.push(new op("TSTA", 1, admod.INH, 1)); opcodes.push(new op("MOV", 3, admod.DD, 5)); opcodes.push(new op("CLRA", 1, admod.INH, 1));
/* 5 */   opcodes.push(new op("NEGX", 1, admod.INH, 1)); opcodes.push(new op("CBEQX", 3, admod.IMM_REL, 4)); opcodes.push(new op("DIV", 1, admod.INH, 7)); opcodes.push(new op("COMX", 1, admod.INH, 1));
      opcodes.push(new op("LSRX", 1, admod.INH, 1)); opcodes.push(new op("LDHX", 2, admod.DIR8, 4)); opcodes.push(new op("RORX", 1, admod.INH, 1)); opcodes.push(new op("ASRX", 1, admod.INH, 1));
      opcodes.push(new op("LSLX", 1, admod.INH, 1)); opcodes.push(new op("ROLX", 1, admod.INH, 1)); opcodes.push(new op("DECX", 1, admod.INH, 1)); opcodes.push(new op("DBNZX", 2, admod.INH, 3));
      opcodes.push(new op("INCX", 1, admod.INH, 1)); opcodes.push(new op("TSTX", 1, admod.INH, 1)); opcodes.push(new op("MOV", 2, admod.DIX_P, 4)); opcodes.push(new op("CLRX", 1, admod.INH, 1));
/* 6 */   opcodes.push(new op("NEG", 2, admod.IX1, 4)); opcodes.push(new op("CBEQ", 3, admod.IX1_P_REL, 5)); opcodes.push(new op("NSA", 1, admod.INH, 3)); opcodes.push(new op("COM", 2, admod.IX1, 4));
      opcodes.push(new op("LSR", 2, admod.IX1, 4)); opcodes.push(new op("CPHX", 3, admod.IMM16, 3)); opcodes.push(new op("ROR", 2, admod.IX1, 4)); opcodes.push(new op("ASR", 2, admod.IX1, 4));
      opcodes.push(new op("LSL", 2, admod.IX1, 4)); opcodes.push(new op("ROL", 2, admod.IX1, 4)); opcodes.push(new op("DEC", 2, admod.IX1, 4)); opcodes.push(new op("DBNZ", 3, admod.IX1, 5));
      opcodes.push(new op("INC", 2, admod.IX1, 4)); opcodes.push(new op("TST", 2, admod.IX1, 3)); opcodes.push(new op("MOV", 3, admod.IMD, 4)); opcodes.push(new op("CLR", 2, admod.IX1, 3));
/* 7 */   opcodes.push(new op("NEG", 1, admod.IX, 3)); opcodes.push(new op("CBEQ", 2, admod.IX_P_REL, 4)); opcodes.push(new op("DAA", 1, admod.INH, 2)); opcodes.push(new op("COM", 1, admod.IX, 3));
      opcodes.push(new op("LSR", 1, admod.IX, 3)); opcodes.push(new op("CPHX", 2, admod.DIR8, 4)); opcodes.push(new op("ROR", 1, admod.IX, 3)); opcodes.push(new op("ASR", 1, admod.IX, 3));
      opcodes.push(new op("LSL", 1, admod.IX, 3)); opcodes.push(new op("ROL", 1, admod.IX, 3)); opcodes.push(new op("DEC", 1, admod.IX, 3)); opcodes.push(new op("DBNZ", 2, admod.IX, 4));
      opcodes.push(new op("INC", 1, admod.IX, 3)); opcodes.push(new op("TST", 1, admod.IX, 2)); opcodes.push(new op("MOV", 2, admod.IX_P_D, 4)); opcodes.push(new op("CLR", 1, admod.IX, 2));
/* 8 */   opcodes.push(new op("RTI", 1, admod.INH, 7)); opcodes.push(new op("RTS", 1, admod.INH, 4)); opcodes.push(new op("", 1, admod.INH, 1)); opcodes.push(new op("SWI", 1, admod.INH, 9));
      opcodes.push(new op("TAP", 1, admod.INH, 2)); opcodes.push(new op("TPA", 1, admod.INH, 1)); opcodes.push(new op("PULA", 1, admod.INH, 2)); opcodes.push(new op("PSHA", 1, admod.INH, 2));
      opcodes.push(new op("PULX", 1, admod.INH, 2)); opcodes.push(new op("PSHX", 1, admod.INH, 2)); opcodes.push(new op("PULH", 1, admod.INH, 2)); opcodes.push(new op("PSHH", 1, admod.INH, 2));
      opcodes.push(new op("CLRH", 1, admod.INH, 1)); opcodes.push(new op("", 1, admod.INH, 1)); opcodes.push(new op("STOP", 1, admod.INH, 1)); opcodes.push(new op("WAIT", 1, admod.INH, 1));
/* 9 */   opcodes.push(new op("BGE", 2, admod.REL, 3)); opcodes.push(new op("BLT", 2, admod.REL, 3)); opcodes.push(new op("BGT", 2, admod.REL, 3)); opcodes.push(new op("BLE", 2, admod.REL, 3));
      opcodes.push(new op("TXS", 1, admod.INH, 3)); opcodes.push(new op("TSX", 1, admod.INH, 3)); opcodes.push(new op("", 1, admod.INH, 1)); opcodes.push(new op("TAX", 1, admod.INH, 1));
      opcodes.push(new op("CLC", 1, admod.INH, 1)); opcodes.push(new op("SEC", 1, admod.INH, 1)); opcodes.push(new op("CLI", 1, admod.INH, 2)); opcodes.push(new op("SEI", 1, admod.INH, 2));
      opcodes.push(new op("RSP", 1, admod.INH, 1)); opcodes.push(new op("NOP", 1, admod.INH, 1)); opcodes.push(new op("", 1, admod.INH, 1)); opcodes.push(new op("TXA", 1, admod.INH, 1));
/* A */   opcodes.push(new op("SUB", 2, admod.IMM, 2)); opcodes.push(new op("CMP", 2, admod.IMM, 2)); opcodes.push(new op("SBC", 2, admod.IMM, 2)); opcodes.push(new op("CPX", 2, admod.IMM, 2));
      opcodes.push(new op("AND", 2, admod.IMM, 2)); opcodes.push(new op("BIT", 2, admod.IMM, 2)); opcodes.push(new op("LDA", 2, admod.IMM, 2)); opcodes.push(new op("AIS", 2, admod.IMM, 2));
      opcodes.push(new op("EOR", 2, admod.IMM, 2)); opcodes.push(new op("ADC", 2, admod.IMM, 2)); opcodes.push(new op("ORA", 2, admod.IMM, 2)); opcodes.push(new op("ADD", 2, admod.IMM, 2));
      opcodes.push(new op("", 1, admod.INH, 1)); opcodes.push(new op("BSR", 2, admod.REL, 4)); opcodes.push(new op("LDX", 2, admod.IMM, 2)); opcodes.push(new op("AIX", 2, admod.IMM, 2));
/* B */   opcodes.push(new op("SUB", 2, admod.DIR8, 3)); opcodes.push(new op("CMP", 2, admod.DIR8, 3)); opcodes.push(new op("SBC", 2, admod.DIR8, 3)); opcodes.push(new op("CPX", 2, admod.DIR8, 3));
      opcodes.push(new op("AND", 2, admod.DIR8, 3)); opcodes.push(new op("BIT", 2, admod.DIR8, 3)); opcodes.push(new op("LDA", 2, admod.DIR8, 3)); opcodes.push(new op("STA", 2, admod.DIR8, 3));
      opcodes.push(new op("EOR", 2, admod.DIR8, 3)); opcodes.push(new op("ADC", 2, admod.DIR8, 3)); opcodes.push(new op("ORA", 2, admod.DIR8, 3)); opcodes.push(new op("ADD", 2, admod.DIR8, 3));
      opcodes.push(new op("JMP", 2, admod.DIR8, 2)); opcodes.push(new op("JSR", 2, admod.DIR8, 4)); opcodes.push(new op("LDX", 2, admod.DIR8, 3)); opcodes.push(new op("STX", 2, admod.DIR8, 3));
/* C */   opcodes.push(new op("SUB", 3, admod.EXT, 4)); opcodes.push(new op("CMP", 3, admod.EXT, 4)); opcodes.push(new op("SBC", 3, admod.EXT, 4)); opcodes.push(new op("CPX", 3, admod.EXT, 4));
      opcodes.push(new op("AND", 3, admod.EXT, 4)); opcodes.push(new op("BIT", 3, admod.EXT, 4)); opcodes.push(new op("LDA", 3, admod.EXT, 4)); opcodes.push(new op("STA", 3, admod.EXT, 4));
      opcodes.push(new op("EOR", 3, admod.EXT, 4)); opcodes.push(new op("ADC", 3, admod.EXT, 4)); opcodes.push(new op("ORA", 3, admod.EXT, 4)); opcodes.push(new op("ADD", 3, admod.EXT, 4));
      opcodes.push(new op("JMP", 3, admod.JEXT, 3)); opcodes.push(new op("JSR", 3, admod.JEXT, 5)); opcodes.push(new op("LDX", 3, admod.EXT, 4)); opcodes.push(new op("STX", 3, admod.EXT, 4));
/* D */   opcodes.push(new op("SUB", 3, admod.IX2, 4)); opcodes.push(new op("CMP", 3, admod.IX2, 4)); opcodes.push(new op("SBC", 3, admod.IX2, 4)); opcodes.push(new op("CPX", 3, admod.IX2, 4));
      opcodes.push(new op("AND", 3, admod.IX2, 4)); opcodes.push(new op("BIT", 3, admod.IX2, 4)); opcodes.push(new op("LDA", 3, admod.IX2, 4)); opcodes.push(new op("STA", 3, admod.IX2, 4));
      opcodes.push(new op("EOR", 3, admod.IX2, 4)); opcodes.push(new op("ADC", 3, admod.IX2, 4)); opcodes.push(new op("ORA", 3, admod.IX2, 4)); opcodes.push(new op("ADD", 3, admod.IX2, 4));
      opcodes.push(new op("JMP", 3, admod.IX2, 4)); opcodes.push(new op("JSR", 3, admod.IX2, 6)); opcodes.push(new op("LDX", 3, admod.IX2, 4)); opcodes.push(new op("STX", 3, admod.IX2, 4));
/* E */   opcodes.push(new op("SUB", 2, admod.IX1, 3)); opcodes.push(new op("CMP", 2, admod.IX1, 3)); opcodes.push(new op("SBC", 2, admod.IX1, 3)); opcodes.push(new op("CPX", 2, admod.IX1, 3));
      opcodes.push(new op("AND", 2, admod.IX1, 3)); opcodes.push(new op("BIT", 2, admod.IX1, 3)); opcodes.push(new op("LDA", 2, admod.IX1, 3)); opcodes.push(new op("STA", 2, admod.IX1, 3));
      opcodes.push(new op("EOR", 2, admod.IX1, 3)); opcodes.push(new op("ADC", 2, admod.IX1, 3)); opcodes.push(new op("ORA", 2, admod.IX1, 3)); opcodes.push(new op("ADD", 2, admod.IX1, 3));
      opcodes.push(new op("JMP", 2, admod.IX1, 3)); opcodes.push(new op("JSR", 2, admod.IX1, 5)); opcodes.push(new op("LDX", 2, admod.IX1, 3)); opcodes.push(new op("STX", 2, admod.IX1, 3));
/* F */   opcodes.push(new op("SUB", 1, admod.IX, 2)); opcodes.push(new op("CMP", 1, admod.IX, 2)); opcodes.push(new op("SBC", 1, admod.IX, 2)); opcodes.push(new op("CPX", 1, admod.IX, 2));
      opcodes.push(new op("AND", 1, admod.IX, 2)); opcodes.push(new op("BIT", 1, admod.IX, 2)); opcodes.push(new op("LDA", 1, admod.IX, 2)); opcodes.push(new op("STA", 1, admod.IX, 2));
      opcodes.push(new op("EOR", 1, admod.IX, 2)); opcodes.push(new op("ADC", 1, admod.IX, 2)); opcodes.push(new op("ORA", 1, admod.IX, 2)); opcodes.push(new op("ADD", 1, admod.IX, 2));
      opcodes.push(new op("JMP", 1, admod.IX, 2)); opcodes.push(new op("JSR", 1, admod.IX, 4)); opcodes.push(new op("LDX", 1, admod.IX, 2)); opcodes.push(new op("STX", 1, admod.IX, 2));

var opcodes9E6 = new Array();
/* 9E6 */ opcodes9E6.push(new op("NEG", 3, admod.SP1, 5)); opcodes9E6.push(new op("CBEQ", 4, admod.SP1_REL, 6)); opcodes9E6.push(new op("", 1, admod.INH, 1)); opcodes9E6.push(new op("COM", 3, admod.SP1, 5));
      opcodes9E6.push(new op("LSR", 3, admod.SP1, 5)); opcodes9E6.push(new op("", 1, admod.INH, 5)); opcodes9E6.push(new op("ROR", 3, admod.SP1, 5)); opcodes9E6.push(new op("ASR", 3, admod.SP1, 5));
      opcodes9E6.push(new op("LSL", 3, admod.SP1, 5)); opcodes9E6.push(new op("ROL", 3, admod.SP1, 5)); opcodes9E6.push(new op("DEC", 3, admod.SP1, 5)); opcodes9E6.push(new op("DBNZ", 4, admod.SP1_REL, 6));
      opcodes9E6.push(new op("INC", 3, admod.SP1, 5)); opcodes9E6.push(new op("TST", 3, admod.SP1, 4)); opcodes9E6.push(new op("", 1, admod.INH, 1)); opcodes9E6.push(new op("CLR", 3, admod.SP1, 4));

var opcodes9ED = new Array();
/* 9ED */ opcodes9ED.push(new op("SUB", 4, admod.SP2, 5)); opcodes9ED.push(new op("CMP", 4, admod.SP2, 5)); opcodes9ED.push(new op("SBC", 4, admod.SP2, 5)); opcodes9ED.push(new op("CPX", 4, admod.SP2, 5));
      opcodes9ED.push(new op("AND", 4, admod.SP2, 5)); opcodes9ED.push(new op("BIT", 4, admod.SP2, 5)); opcodes9ED.push(new op("LDA", 4, admod.SP2, 5)); opcodes9ED.push(new op("STA", 4, admod.SP2, 5));
      opcodes9ED.push(new op("EOR", 4, admod.SP2, 5)); opcodes9ED.push(new op("ADC", 4, admod.SP2, 5)); opcodes9ED.push(new op("ORA", 4, admod.SP2, 5)); opcodes9ED.push(new op("ADD", 4, admod.SP2, 5));
      opcodes9ED.push(new op("", 1, admod.INH, 1)); opcodes9ED.push(new op("", 1, admod.INH, 1)); opcodes9ED.push(new op("LDX", 4, admod.SP2, 5)); opcodes9ED.push(new op("STX", 4, admod.SP2, 5));

var opcodes9EE = new Array();
/* 9EE */ opcodes9EE.push(new op("SUB", 3, admod.SP1, 4)); opcodes9EE.push(new op("CMP", 3, admod.SP1, 4)); opcodes9EE.push(new op("SBC", 3, admod.SP1, 4)); opcodes9EE.push(new op("CPX", 3, admod.SP1, 4));
      opcodes9EE.push(new op("AND", 3, admod.SP1, 4)); opcodes9EE.push(new op("BIT", 3, admod.SP1, 4)); opcodes9EE.push(new op("LDA", 3, admod.SP1, 4)); opcodes9EE.push(new op("STA", 3, admod.SP1, 4));
      opcodes9EE.push(new op("EOR", 3, admod.SP1, 4)); opcodes9EE.push(new op("ADC", 3, admod.SP1, 4)); opcodes9EE.push(new op("ORA", 3, admod.SP1, 4)); opcodes9EE.push(new op("ADD", 3, admod.SP1, 4));
      opcodes9EE.push(new op("", 1, admod.INH, 1)); opcodes9EE.push(new op("", 1, admod.INH, 1)); opcodes9EE.push(new op("LDX", 3, admod.SP1, 4)); opcodes9EE.push(new op("STX", 3, admod.SP1, 4));
