; ese.asm
; confronta OK (messo in A) con ERR  ->>> salta x maggiore uguale non salta ma salta per minore
; autore: Stefano Salvi
; CCR:
; V x x H I N Z C
;
; DISPLAY:
;  DB4       15          PTB0
;  DB5       14          PTB1
;  DB6       11          PTB2
;  DB7       10          PTB3
;
;  EN_LCD    3           PTB6                LED Verde
;  RS_LCD    5           PTA4                LED Giallo
;
;  S5        2           PTB7                Pulsante centrale
;
STARTPGM  = 0xee00    ; L'inizio dell'ipotetica ROM
RESETVECT = 0xfffe    ; La posizione del vettore di reset
STARTDATI = 0x80    ; Inizio area dati


PTA             =       0x0000
PTB             =       0x0001
DDRA            =       0x0004
DDRB            =       0X0005
PTAPUE          =       0X000B
PTBPUE          =       0X000C
CONFIG2         =       0x001e
CONFIG1         =       0x001f
;
STARTPGM        =       0xee00
RESETVECT       =       0xfffe
STARTDATI       =       0x0080


    .area DATI (ABS)
    .org  STARTDATI
OK:   .blkb   1
ERR:  .blkb   1
c1:   .blkb   1
c2:   .blkb   1

    .area   PROGRAMMA (ABS)
    .org    STARTPGM

MAIN:
; ***********************
; Setup processore e porte di I/O
; ***********************
        MOV     #0x31,*CONFIG1  ; LVIRSTD, VIPWRD, COPD
        CLR     *CONFIG2        ; Tutti i flag spenti
        MOV     #0x10,*PTA      ; PTA4 alto (LED Giallo)
        MOV     #0x10,*DDRA     ; PTA4 in output (LED Giallo)
        MOV     #0x00,*PTBPUE   ; Disattiva tutte le resistenze di pull-up sugli ingressi
        MOV     #0xcf,*PTB      ; Il valore richiesto sul fili di uscita
        MOV     #0xcf,*DDRB     ; PTB6, PTB7 e PTB0-3 in output (per ora anche il pulsante)
        MOV     #0x4f,*DDRB     ; PTB6 e PTB0-3 in output (per ora anche il pulsante)
        MOV     #0x80,*PTBPUE   ; Attiva le resistenze di pull-up sul tasto

    LDA #0
    STA OK
    STA ERR

; ***********************
; BGE: (0x90)
; Branch if ACCUMULATOR is greather or equl to <operand>
; ***********************
    LDA #32
    CMP #32       ; Z
    BGE BGEEQ   ; A == OPR (jump)
    INC ERR
    BRA BGEEQFINE
BGEEQ:
    INC OK
BGEEQFINE:

    CMP #16           ; -
    BGE BGEMORE       ; A > OPR (jump)
    INC ERR
    BRA BGEMOREFINE
BGEMORE:
    INC OK
BGEMOREFINE:

    CMP #48           ; C
    BGE BGELESS       ; A < OPR (no jump)
    INC OK
    BRA BGELESSFINE
BGELESS:
    INC ERR
BGELESSFINE:

    LDA #-32

    CMP #16           ; V N
    BGE BGELESSN      ; A < OPR (no jump)
    INC OK
    BRA BGELESSNFINE
BGELESSN:
    INC ERR
BGELESSNFINE:

    CMP #-16          ; N C
    BGE BGELESSNN     ; A < OPR (no jump)
    INC OK
    BRA BGELESSNNFINE
BGELESSNN:
    INC ERR
BGELESSNNFINE:

    CMP #-48          ; -
    BGE BGEMORENN     ; A > OPR (jump)
    INC ERR
    BRA BGEMORENNFINE
BGEMORENN:
    INC OK
BGEMORENNFINE:


; ***********************
; BLT: (0x91)
; Branch if ACCUMULATOR is less than <operand>
; ***********************
    LDA #32
    CMP #32           ; Z
    BLT BLTEQ         ; A == OPR (no jump)
    INC OK
    BRA BLTEQFINE
BLTEQ:
    INC ERR
BLTEQFINE:

    CMP #16           ; -
    BLT BLTMORE       ; A > OPR (jump)
    INC OK
    BRA BLTMOREFINE
BLTMORE:
    INC ERR
BLTMOREFINE:

    CMP #48           ; C
    BLT BLTLESS       ; A < OPR (no jump)
    INC ERR
    BRA BLTLESSFINE
BLTLESS:
    INC OK
BLTLESSFINE:

    LDA #-32

    CMP #16           ; V N
    BLT BLTLESSN      ; A < OPR (no jump)
    INC ERR
    BRA BLTLESSNFINE
BLTLESSN:
    INC OK
BLTLESSNFINE:

    CMP #-16          ; N C
    BLT BLTLESSNN     ; A < OPR (no jump)
    INC ERR
    BRA BLTLESSNNFINE
BLTLESSNN:
    INC OK
BLTLESSNNFINE:

    CMP #-48          ; -
    BLT BLTMORENN     ; A > OPR (jump)
    INC OK
    BRA BLTMORENNFINE
BLTMORENN:
    INC ERR
BLTMORENNFINE:

; ***********************
; BGT: (0x92)
; Branch if ACCUMULATOR is greter than <operand>
; ***********************
    LDA #32
    CMP #32       ; Z
    BGT BGTEQ   ; A == OPR (jump)
    INC OK
    BRA BGTEQFINE
BGTEQ:
    INC ERR
BGTEQFINE:

    CMP #16           ; -
    BGT BGTMORE       ; A > OPR (jump)
    INC ERR
    BRA BGTMOREFINE
BGTMORE:
    INC OK
BGTMOREFINE:

    CMP #48           ; C
    BGT BGTLESS       ; A < OPR (no jump)
    INC OK
    BRA BGTLESSFINE
BGTLESS:
    INC ERR
BGTLESSFINE:

    LDA #-32

    CMP #16           ; V N
    BGT BGTLESSN      ; A < OPR (no jump)
    INC OK
    BRA BGTLESSNFINE
BGTLESSN:
    INC ERR
BGTLESSNFINE:

    CMP #-16          ; N C
    BGT BGTLESSNN     ; A < OPR (no jump)
    INC OK
    BRA BGTLESSNNFINE
BGTLESSNN:
    INC ERR
BGTLESSNNFINE:

    CMP #-48          ; -
    BGT BGTMORENN     ; A > OPR (jump)
    INC ERR
    BRA BGTMORENNFINE
BGTMORENN:
    INC OK
BGTMORENNFINE:

; ***********************
; BLE: (0x93)
; Branch if ACCUMULATOR is less than or equl to <operand>
; ***********************
    LDA #32
    CMP #32
    BLE BLEEQ   ; A == OPR (jump) (Z)
    INC ERR
    BRA BLEEQFINE
BLEEQ:
    INC OK
BLEEQFINE:

  CMP #16
  BLE BLEMORE  ; A > OPR (no jump)
  INC OK
  BRA BLEMOREFINE
BLEMORE:
  INC ERR
BLEMOREFINE:

  CMP #48
  BLE BLELESS  ; A < OPR (jump)
  INC ERR
  BRA BLELESSFINE
BLELESS:
  INC OK
BLELESSFINE:

    LDA #-32

    CMP #16           ; -
    BLE BLELESSN   ; A < OPR (jump)
    INC ERR
    BRA BLELESSNFINE
BLELESSN:
    INC OK
BLELESSNFINE:

    CMP #-16          ; C
    BLE BLELESSNN   ; A < OPR (jump)
    INC ERR
    BRA BLELESSNNFINE
BLELESSNN:
    INC OK
BLELESSNNFINE:

    CMP #-48          ; C
    BLE BLEMORENN   ; A > OPR (no jump)
    INC OK
    BRA BLEMORENNFINE
BLEMORENN:
    INC ERR
BLEMORENNFINE:

; Loop finale per arrestare il programma
FINE: BRA FINE
;
  .area   RESET (ABS)
  .org    RESETVECT
  .word   MAIN
