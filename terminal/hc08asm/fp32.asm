;***********************************************************************************
; LISTING NO.: 1
;
; FILE NAME: FP4.ASM
; PURPOSE: To provide a FLASH erase, program and verify program
; TARGET DEVICE: HC908GP20
;
; MEMORY USAGE - RAM: 1A0H BYTES
;			ROM: 280H BYTES
;
; ASSEMBLER: CASM08
; VERSION: 1.02
;
; PROGRAM DESCRIPTION:
; This program loads a RAM routine with instructions/data
; located in FLASH memory that:
;	Receives data over the SCI or Port A, bit 0
;	Row-erases FLASH block if necessary
;	Programs FLASH with received data
;	Dumps specified row out comm port
;	Bulk erases device upon command
;
; The program has assembler directives to be able to program in
; both user and monitor modes. In monitor mode, the generated S-record
; file will contain all of the necessary programming routines in RAM. It
; will not have any code that would reside out of RAM. In user mode, load
; routines are incorporated so that it could be contained in a user's
; application. The load routines load the programming routines into RAM and
; from there it looks just like the RAM routine executed in monitor mode.
;
;
; AUTHOR: Grant Whitacre
; LOCATION: Austin, Texas
;
; UPDATE HISTORY:
; REV	AUTHOR		DATE		DESCRIPTION OF CHANGE
; ===	============	========	=====================
; 0.0	GRANT WHITACRE	03/04/98	INITIAL VERSION
; 0.1	GRANT WHITACRE	04/15/98	SECOND VERSION (FP2.ASM)-
;					LOADS ALL RTNS INTO RAM
;					SO RE-ENTRY INTO FLASH IS
;					NOT NECESSARY. ALLOWS
;					REPROGRAMMING OF ENTIRE
;					FLASH ARRAY.
; 0.2	GRANT WHITACRE	06/22/98	CONSOLIDATES PROGRAM TO
;					ALLOW BOTH USER MODE AND
;					MONITOR MODE PROGRAMMING
;					SELECTABLE BY ASSEMBLER
;					DIRECTIVES
;
;
; GENERAL CODING NOTES:
; Bit names are labeled with <port name><bit number> and are
; used in the commands that operate on individual bits, such
; as BSET and BCLR. A bit name followed by a dot indicates
; a label that will be used to form a bit mask.
;
; FOR TESTING, HAD TO ADD AN ASSEMBLER SWITCH (TESTMOD) TO
; KEEP FROM TRIPPING ON AN ILLEGAL MEMORY WRITE BREAK IN
; THE DEBUGGER. OTHER SWITCHES ARE INSTALLED FOR EASE OF
; TESTING.
;
;***********************************************************************************
; ASSEMBLER DIRECTIVES
; (INCLUDES, BASE, MACROS, SETS, CONDITIONS, RAM DEFS, ETC.)
;***********************************************************************************
	       .radix	D			;DEFAULT TO BASE 10 NUMBER DESIGNATION
						;Remember: ACTIVE LOW!!!!!!!!!!!!!!!!!!
MONPROG		=	0				;IF SET, ALL (NECESSARY) ROUTINES WILL
						;BE ADDRESSED IN RAM INITIALLY; THIS
						;VERSION WOULD BE USED AS THE S19 RECORD
						;FILE THAT IS DOWNLOADED INTO RAM IN
						;MONITOR MODE FOR FLASH PROGRAMMING
;Be sure to manually set addresses of GET_PUT and PUT_BYTE if TESTMOD set!!
TESTMOD		=	0				;SINCE WE GET AN ILLEGAL WRITE ERROR
						;USING THE MMDS WHEN WE TRY TO WRITE TO
						;EMULATED MEMORY, TESTMOD CAUSES A READ
						;FROM FLASH LOCATION INSTEAD OF A WRITE
						;TO IT. OF COURSE VERIFY NEVER WORKS
						;UNDER THESE CIRCUMSTANCES. TURN OFF
						;TEST MODE FOR REAL TARGET EXECUTION.
ERSDTST		=	0		        ;SET TO FORCE A GOOD VERIFICATION OF
						;ERASED STATE
;***********************************************************************************
; PORT AND I/O REGISTER EQUATES
;***********************************************************************************
PORTA		=	$$00			;I/O PORT A
PORTB		=	$$01			;I/O PORT B
PORTC		=	$$02			;I/O PORT C
PORTD		=	$$03			;I/O PORT D
PORTE		=	$$07			;I/O PORT E
;***** FLASH CONTROL REGISTER ******************************************************
FLCR		=	$$FE08		;FLASH CONTROL REGISTER
; FDIV1		=	7
; FDIV0		=	6
; BLK1		=	5
; BLK0		=	4
HVEN		=	3
; VERF		=	2			;FOR 908GP20 ONLY
; MARG		=	2			;FOR 908XL36 ONLY
MASS            =       2                       ;FOR 908GP32
ERASE		=	1
PGM		=	0
; FDIV1.          =	$$80
; FDIV0.          =	$$40
; BLK1.		=	$$20
; BLK0.		=	$$10
HVEN.		=	$$08
; VERF.		=	$$04
; MARG.		=	$$04
MASS.           =       $$04
ERASE.          =	$$02
PGM.		=	$$01
;***** BLOCK PROTECTION REGISTER ***************************************************
; FLBPR		=	$$FF80		;FLASH BLOCK PROTECTION - This is for HC08HC908GP20
FLBPR		=	$$FF7E		;FLASH BLOCK PROTECTION - This is the right address for HC08HC908GP32
					;REGISTER
;***********************************************************************************
; APPLICATION-SPECIFIC MEMORY AND I/O EQUATES
;***********************************************************************************
; THE VALUE FOR CPUSPD DRIVES THE FDIV SETTING AND THE BAUD RATE
; PRESCALER FOR THE SCI.
; MAKE SURE THAT CPUSPD IS SET TO 2 IF THE PLL IS TO BE USED.
; IN MONITOR MODE THE BUS FREQUENCY IS 2.45 MHZ, SO USE 2
CPUSPD          =	2			;2 = 2.45 MHZ OPER. FREQ.
						;4 = 4.92 MHZ, 8 = 8.0 MHZ
; ABS. ADDRESS OF MONITOR ROUTINES COMMENTED OUT IN TEST VERSION
GET_PUT         =	$$FE97		        ;MON RTN TO GET A BYTE ON
						;PTA0 AND ECHO BACK
PUT_BYTE	=	$$FEAA		        ;SEND A BYTE OUT PTA0
MONRTNS	        =	$$B300		        ;MONITOR ROUTINES INCLUDED IN
						;PROGRAM FOR TESTING
RSTVLOC	        =	$$FFFE		        ;RESET VECTOR LOCATION
RAM		=	$$50			;FIRST ADDRESS OF RAM, ACTUALLY
						;$$40 BUT WILL START AT $50
RAMEND          =       $$240                   ;-- Salvi : RAM End
NXTPAGE         =	$$100
STCKSIZ         =	$$18
PRGSTRT         =	$$F000		        ;START OF FLASH PROGRAM
LASTBYT         =	$$F0F0
NXFPAGE         =	$$F100
RAMPRG          =	RAM+$$58		;START OF RAM ROUTINE
RAMPRSZ         =	$$197			;320 BYTES (MAX)
TESTDAT         =	$$C0
XFRCODE         =	PRGSTRT+RAMPRG
STUBINT         =	PRGSTRT+$$240		;PUT HERE FOR NOW -
						;SHOULD BE SAFE
VECSTRT         =	$$FFDC	                ;START OF USER VECTOR AREA
PRGTRIES	=	25			;MAX. NUMBER OF PROGRAM TRIES
; MASKS FOR ERASE RANGE
FARMASK         =	$$00			;FULL ARRAY
HARMASK         =	$$10			;HALF ARRAY
RW8MASK         =	$$20			;8 ROWS
ROWMASK         =	$$30			;1 ROW
; PROGRAMMING TIMES - CHANGE THESE VALUES IF NECESSARY TO
; CHANGE TIMES! ALL TIMES ARE IN MICROSECONDS
VTPGM		=	1000			;1000 - PROGRAM TIME
VHLFTER         =	50000		        ;1/2 ERASE TIME
VTKILL	        =	200			;HIGH-VOLTAGE KILL TIME
VTHVD		=	50			;RETURN TO READ TIME
VTHVTV          =	50			;HVEN LOW TO VERF HIGH TIME
VTVTP		=	150			;VERF HIGH TO PGM LOW TIME
; INTERMEDIATE PROGRAMMING TIMES (CALCULATION PURPOSES ONLY)
CTPGM		=	(VTPGM/6)		;DIVIDE BY 6 HERE TO NORMALIZE
CHLFTER         =	(VHLFTER/6)		;FOR NEXT STEP. PADDED FROM
CTKILL          =	(VTKILL/6)		;8 TO 6 TO COMPENSATE FOR ODD
CTHVD		=	(VTHVD/6)		;FREQUENCIES (2.45 AND 4.92 MHZ)
CTHVTV          =	(VTHVTV/6)		;AND TRUNCATION.
CTVTP		=	(VTVTP/6)
; CPU SPEED-CORRECTED PROGRAMMING TIMES (TIMES ACTUALLY USED)
TPGM		=	(CPUSPD*CTPGM)          ;Program time = 1000 µs @ 8 MHz
HLFTERA         =	(CPUSPD*CHLFTER)        ;Half of Erase time = 50 µs @ 8 MHz
TKILL		=	(CPUSPD*CTKILL)         ;HV Kill time = 200 µs
THVD		=	(CPUSPD*CTHVD)          ;Return to read time = 50 µs
THVTV		=	(CPUSPD*CTHVTV)         ;HVEN low to VERF high time = 50 µs
TVTP		=	(CPUSPD*CTVTP)          ;VERF high to PGM low time = 150 µs
;***********************************************************************************
; VARIABLE DEFINITIONS & RAM SPACE USAGE
;***********************************************************************************
; $$40-$4F	NOT USED			( 16 BYTES)
; $$50		TRANSFER SIZE			( 1 BYTE)
; $$51-$52	FIRST ADDRESS TO BE PROGRAMMED( 2 BYTES)
; $$53		DATA SIZE (DATASIZ)		( 1 BYTE)
; $$54-$93	DATA ARRAY			( 64 BYTES)
; $$94-$A7	VARIABLES			( 20 BYTES)
; $$A8-$EF	RAM PROGRAM			( 72 BYTES)
; $$F0-$FF	STACK				( 16 BYTES)
; $$100-$23F	RAM PROGRAM			(320 BYTES)
; $$50-$23F	TOTAL				(512 BYTES)
		.area	RAMPGM	(ABS)
		.org	RAM
XFRSIZE:	.blkb	1			;NUMBER OF BYTES TO BE TRANSFERRED
FRSTADR:	.blkb	2			;FIRST ADDR TO BE PROGRAMMED
DATASIZ:	.blkb	1			;NUMBER OF BYTES TO PROGRAM
DATARAY:	.blkb	64			;RESERVE 64 BYTES FOR DATA
DATA1:		.blkb	8			;DATA TO BE PROGR. IN PAGE-8 BYTES
REPROG:		.blkb	1			;A $$01 HERE SIGNALS NEED TO REPROGRAM
TEMPH:		.blkb	1			;RAM COUNTER (HI BYTE)
TEMPL:		.blkb	1			;RAM COUNTER (LOW BYTE)
TEMP2:		.blkb	1			;TEMP. HOLDING LOC. FOR TRANSFERS/PR
						;TRIES
FDIVMSK:	.blkb	1			;FDIV OF FLCR BIT MASK
BYTECNT:	.blkb	1			;BYTE COUNT USED DURING PAGE PROG.
ARAYIDX:	.blkb	1			;INDEX INTO THE DATA ARRAY
CURRBYT:	.blkb	1			;CURRENT BYTE BEING READ/WRITTEN (0-7)
STATBYT:	.blkb	1			;STATUS OF PROGRAM SUCCESS -
						;BIT 7 = FAILED; BIT 6 = SUCCEEDED
;***********************************************************************************
;	Program Algorithm (User Mode Programming)
;	1.	Initialize all variables and ports, PLL (if selected) and
;		SCI.
;	2.	Monitor SCI port for input of block of data to be
;		programmed and the start address. Load RAM with the data
;		array (up to 64 bytes), the start address and length of
;		data array.
;	3.	Transfer the following subroutines to
;		RAM at address RAMPRG
;		A.	LDDATA
;		B.	MAINPRG
;		C.	ERABLK
;		D.	DELNUS
;		E.	PRGPAGE
;	4.	Jump to first byte of main RAM program (RAMPRG).
;	5	Execute RAM program MAINPRG and then return to SCI
;		port monitoring loop in RAM.
;
;	Program Algorithm - Monitor Mode Programming
;	1.	Monitor PTA0 for input of block of data to be
;		programmed and the start address. Load RAM with the data
;		array (up to 64 bytes), the start address and length of
;		data array.
;	2.	Execute RAM program MAINPRG and then return to PTA0
;		monitoring loop in RAM.
;***********************************************************************************
;
; START OF PROGRAM
;***********************************************************************************
; START OF THE MONITOR PROGRAM WHICH WE'LL .org IN RAM
		.org	RAMPRG		;CURRENTLY $$A8
START:
;                LDHX    RAMEND
;                TXS                     ; Sets the upper limit of accessible RAM
;***********************************************************************************
; NAME: LDDATA
; PURPOSE: LOAD RAM WITH USER'S DATA AND START ADDRESS VIA THE SCI;
;          PROGRAMS AND THEN DUMPS DATA THAT IS DOWNLOADED; ONLY DUMPS DATA
;          IN ROW SPECIFIED IF NUMBER OF BYTES TO BE PROGRAMMED (DATASIZ) IS 0.
; ENTRY CONDITIONS:
; EXIT CONDITIONS:
; SUBROUTINES CALLED: PRGFLSH, DUMPROW
; EXTERNAL VARIABLES USED:
; DESCRIPTION: EXECUTED OUT OF RAM
; THE STRUCTURE OF THE DATA RECEIVED IS AS FOLLOWS:
;	LOCATION	DESCRIPTION				RAM LOC.
;	========	==================================	========
;	1		COUNT OF THE TOTAL NUMBER OF		$$40
;			BYTES TO BE SENT (INCL. THAT BYTE)
;	2-3		THE FIRST ADDRESS WHERE THE		$$41-$42
;			FOLLOWING DATA IS TO BE PROGRAMMED
;	4		NUMBER OF BYTES TO BE PROGRAMMED	$$43
;	5-68		ARRAY SPACE FOR DATA TO BE PROGRAMMED	$$44-$83
;
; IF A COUNT IS USED THAT IS GREATER THAN (PROGRAM LENGTH + 1)
; THEN THE ROUTINE WILL HANG AFTER THE LAST PROGRAM BYTE IS SENT.
; CONTINUOUSLY LOOPS LOOKING FOR NEW DATA ON THE SCI. MUST RESET
; AFTER THE LAST ROW DOWNLOAD.
; IF A DATA ARRAY IS RECEIVED WITH A NUMBER OF BYTES TO BE PROGRAMMED OF >= $$80
; THEN PROGRAM WILL CONSTRUE THIS AS A SIGNAL TO ERASE THE ENTIRE ARRAY. THIS
; WAS THE MOST CONVENIENT WAY TO IMPLEMENT BULK ERASE WITHOUT HAVING TO HAVE
; A COMMAND BYTE IN THE DATA STRUCTURE.
;***********************************************************************************
LDDATA:		CLRH
		CLRA
		LDX	#RAM			;POINT TO START OF RAM
		CLR	FDIVMSK
WAITRX:
		JSR	GET_PUT
		NOP
		NOP
CHKCHK:	        CPX	#RAM			;IF VALUE OF 1ST BYTE IS ZERO, THEN
		BNE	STORNOW		        ;BAD START - KEEP LOOPING FOR NON-
		TSTA                            ;ZERO FIRST BYTE
		BEQ	WAITRX
STORNOW:	STA	,X			;STORE THE DATA IN RAM
		INCX				;MOVE TO NEXT RAM LOCATION
		DBNZ	*RAM,WAITRX		;DEC. PROG SIZE CNTR (1st BYTE)
						;IF ENTIRE PROG NOT LODED, CONT.
		LDA	DATASIZ	                ;IF SIZE OF DATA TO BE PROGRAMMED
		BEQ	DUMP			;IS 0, THEN ONLY DUMP THIS ROW.
		BPL	JUSTPRG
		CLRA
		ORA	#FARMASK
		BSR	JERASE                  ;ERASEIT
		BRA	DUMP
JUSTPRG:	
		LDHX	FRSTADR
                BSR     JPRGPAGE
;;                BSR	PRGFLSH
DUMP:		BSR	DUMPROW
		BRA	LDDATA
;***********************************************************************************
; NAME: DUMPROW
; PURPOSE: DUMPS THE ENTIRE 64-BYTE ROW THAT THE START ADDR IS IN
; ENTRY CONDITIONS: H-X CONTAINS THE NEXT ADDR TO BE PROGRAMMED
; EXIT CONDITIONS: NONE
; SUBROUTINES CALLED: ERACHK
; EXTERNAL VARIABLES USED:
; DESCRIPTION: EXECUTED OUT OF RAM
;***********************************************************************************
DUMPROW:	LDHX	FRSTADR	                ;PUT FIRST ADDR IN H-X
		TXA
		AND	#$$C0			;PUT ON ROW BOUNDARY
		TAX
RDBYTE:	LDA	,X
WAITTX:
		JSR	PUT_BYTE		;SEND OUT PTA0
		NOP
		NOP
		NOP
		NOP
		NOP
		NOP
		INCX				;MOVE TO NEXT ADDRESS
		TXA
		AND	#$$3F
		BNE	RDBYTE                  ;IF NOT FINISHED, CONTINUE
ENDDUMP:	RTS
;***********************************************************************************
; FOLLOWING IS RELOCATABLE, DEPENDING ON WHERE THE PAGE BREAK FOR THE
; STACK IS TO BE LOCATED. MUST PLACE IT SO THAT THE BRANCH TO THE NEXT
; PAGE IS AT LEAST PAST $$FF.
JERASE:         BSR	ERASEIT		        ;NEED A 1/2 BRANCH HERE
LASTBYTE:	RTS
;
;		.org	LASTBYTE+STCKSIZ	;STCKSIZ NEEDS TO BE SET AS EQUATE
                .ds STCKSIZ
;***********************************************************************************
JPRGPAGE:       BSR     PRGPAGE
                RTS
;***********************************************************************************
; NAME: PRGFLSH
; PURPOSE: ERASES (IF NECESSARY), PROGRAMS DATA IN DATA ARRAY
;	AT LOCATION SPECIFIED, AND THEN VERIFIES.
;	DATA MUST BE WITHIN A ROW BOUNDARY.
; ENTRY CONDITIONS: NONE
; EXIT CONDITIONS: NONE
; SUBROUTINES CALLED: JERACH (ERACHK), JPRGPAG (PRGPAGE)
; EXTERNAL VARIABLES USED:
; DESCRIPTION: EXECUTED OUT OF FLASH
;		PROGRAMMING ALGORITHM
;	1. LOAD FIRST ADDRESS; CLEAR BYTCNTR
;	2. SEE IF ADDRESS IS ON PAGE BOUNDARY. IF NOT,
;	LOAD EXISTING DATA FROM CORRECT LOCATIONS IN FLASH
;	TO FILL IN THE BEGINNING OF THE PAGE.
;	3. LOAD DATA FROM DATA BUFFER TO FILL (REST OF) PAGE OR
;	UNTIL THE END OF THE DATA ARRAY IS REACHED, WHICHEVER
;	COMES FIRST.
;	4. IF DATASIZ WAS NOT BIG ENOUGH TO COMPLETE CURRENT PAGE,
;	THEN LOAD EXISTING DATA FROM CORRECT LOCATIONS IN
;	FLASH TO FILL IN THE END OF THE PAGE.
;	5. INCREMENT BYTECNT ACCORDING TO HOW MANY BYTES OF DATA
;	WERE USED FOR CURRENT PAGE.
;	6. PROGRAM PAGE WITH THE EIGHT BYTES OF LOADED DATA.
;	7. PERFORM VERIFICATION CHECK TO SEE IF PAGE WAS PROGRAMMED.
;	IF NOT, THEN SEND A HIGH OUT THE "VERIFICATION FAILED" PORT
;	AND RETURN TO FLASH.
;	8. CHECK TO SEE IF BYTECNT = DATASIZ. IF SO THEN RETURN
;	TO FLASH. IF NOT, GO TO 3.
; THIS ROUTINE CHECKS TO SEE IF THE FIRST ADDRESS OF THE DATA
; TO BE PROGRAMMED IS ON A PAGE BOUNDARY, AND THE LAST ADDRESS
; OF DATA TO BE PROGRAMMED IN AT THE END OF A PAGE. SINCE WE PROGRAM
; WHOLE PAGES AT A TIME, WE'LL JUST REPROGRAM THOSE BYTES IN THE
; PAGE BEFORE THE FIRST ADDRESS (IF NECESSARY) AND THOSE BYTES IN
; THE PAGE AFTER THE LAST ADDRESS (IF NECESSARY) WITH THE VALUE
; THAT ALREADY EXISTS THERE IN FLASH.
;***********************************************************************************
PRGFLSH:
;---;		BSET	#1,*PORTB
;---;		CLRA
;---;		ORA	FDIVMSK
;---;		STA	FLCR
;---;		BSR	ERACHK
;---;		CLR	BYTECNT
;---;		LDHX	FRSTADR
;---;		PSHX
;---;		PSHH
;---;NOSTUFF:
;---;; FOLLOWING LOADS REST	OF OR ALL 8 BYTES INTO THE DATA BUFFER
;---;		CLR	ARAYIDX
;---;NXTLOAD:	CLRH
;---;		LDX	ARAYIDX
;---;		CPX	DATASIZ		        ;SEE IF ALL DATA IN
;---;						;ARRAY HAS BEEN LOADED
;---;		BEQ	NOMODAT
;---;		LDA	DATARAY,X		;WHERE X CONTAINS INDEX INTO ARRAY
;---;		INC	ARAYIDX
;---;		LDX	BYTECNT
;---;		STA	DATA1,X
;---;		INCX
;---;		STX	BYTECNT
;---;		CPX	#08
;---;		BNE	NXTLOAD
;---;		PULH
;---;		PULX
;---;		BSR	JPRGPAG
;---;		PSHX
;---;		PSHH
;---;		CLR	BYTECNT
;---;		BRA	NXTLOAD
;---;NOMODAT:	PULH
;---;		PULX
		RTS
;***********************************************************************************
; NAME: ERACHK
; PURPOSE: CHECKS TO SEE IF RANGE TO BE PROGRAMMED NEEDS TO BE
; ERASED FIRST, AND ERASES IF NECESSARY. ERASE BITS (BLK0, BLK1)
; AND FDIV BITS ALREADY SET IN FPCR; VALID ADDRESS FOR THIS RANGE
; LOADED IN H:X. THIS ROUTINE DOES NOT VERIFY ERASE, AND THEN DO A NUMBER
; OF ATTEMPTS TO RE-ERASE. MAYBE LATER IF ENOUGH ROOM.
; ENTRY CONDITIONS: NONE
; EXIT CONDITIONS: NONE
; SUBROUTINES CALLED: DELNUS
; EXTERNAL VARIABLES USED:
; DESCRIPTION: EXECUTED OUT OF RAM
; IF THE RANGE TO BE PROGRAMMED IS NOT ALREADY ERASED, THIS
; ROUTINE WILL AUTOMATICALLY ERASE THE ROW THAT THIS DATA IS IN.
;***********************************************************************************
ERACHK:         MOV 	*DATASIZ,BYTECNT        ;WANT TO KEEP THIS VALUE
;---;						;USED AS A COUNTER HERE
;---;                LDHX	FRSTADR		                ;LOAD THE FIRST ADDR IN H:X
;---;NXTCHK:         LDA	,X				;LOAD THE DATA AT THIS ADDR
;---;                .if	ERSDTST
;---;		CLRA
;---;                .endif
;---;		BNE	ERAROW	                ;IF NOT ZERO, THEN ERASE ROW
;---;		AIX	#01
;---;		DBNZ	*BYTECNT,NXTCHK
;---;		BRA	XERACHK
;---;ERAROW:		CLRA
;---;		ORA	#ROWMASK
ERASEIT:	LDHX	FRSTADR	                ;RELOAD ADDRESS IN CASE ENTRY WAS AT ERASEIT
;---;		ORA	FDIVMSK
;---;		ORA	#ERASE.
;---;		STA	FLCR			;SET FDIV MASK BASED ON CPU SPEED
;---;ERABLK:		LDA	FLBPR
;---;	.if	TESTMOD
;---;		LDA	,X
;---;	.else
;---;		STA	,X
;---;	.endif
;---;		LDHX	#FLCR	                ;FOLLOWING SETS THE HVEN BIT IN FLCR
;---;		LDA	,X
;---;		ORA	#HVEN.
;---;		STA	,X
;---;		LDHX	#HLFTERA		;DELAY FOR 1/2 OF TERA
;---;		BSR	DELNUS
;---;		LDHX	#HLFTERA		;DELAY FOR 1/2 OF TERA
;---;		BSR	DELNUS
;---;		LDHX	#FLCR	                ;CLEARS THE HVEN BIT
;---;		LDA	,X
;---;		EOR	#HVEN.
;---;		STA	,X
;---;		LDHX	#TKILL		        ;DELAY FOR TKILL
;---;		BSR	DELNUS
;---;		LDHX	#FLCR		        ;CLEAR ERASE BIT
;---;		LDA	,X
;---;		EOR	#ERASE.
;---;		STA	,X
;---;		LDHX	#THVD                   ;DELAY FOR THVD
;---;		BSR	DELNUS
;---;XERACHK:	RTS
;---;JPRGPAG:	BSR	PRGPAGE                 ;NEEDED TO STAY IN RANGE
		RTS
;************************************************************
; NAME: DELNUS
; PURPOSE: DELAY N µs
; ENTRY CONDITIONS: H-X CONTAINS THE TIME DELAY (IN µs.)
; EXIT CONDITIONS: PRESERVES THE CONTENTS OF ACC
; SUBROUTINES CALLED: NONE
; EXTERNAL VARIABLES USED:
; DESCRIPTION: EXECUTED OUT OF RAM
;************************************************************
DELNUS:		PSHA
		PSHH
		PULA
; FOLLOWING LOOP EXECUTES H-X NUMBER OF TIMES; 99.6% OF THE
; TIME THIS LOOP IS 8 CYCLES IN DURATION WHICH IS 1 uS @ 8 MHz
D1US:           TSTX				;(1) X
		BNE	NOADEC		        ;(3) X
		TSTA				;(1)
		BEQ	XDELNUS	                ;(3)
		DECA				;(1)
NOADEC:		DECX				;(1) X
		BRA	D1US			;(3) X
XDELNUS:        PULA
		RTS				;(4) RETURN AFTER WANTED DELAY
;***********************************************************************************
; NAME: PRGPAGE
; PURPOSE: PROGRAMS A PAGE (8 BYTES) OF FLASH
; ENTRY CONDITIONS: H-X REG. LOADED WITH FIRST ADDRESS TO BE
; PROGRAMMED; Programs data in DATARAY
; EXIT CONDITIONS: NONE
; SUBROUTINES CALLED: DELNUS
; EXTERNAL VARIABLES USED:
; DESCRIPTION: EXECUTED OUT OF RAM
;
; 1. Set the PGM bit. This configures the memory for program
;    operation and enables the latching of address and data for
;    programming.
; 2. Read from the FLASH block protect register.
; 3. Write any data to any FLASH address within the row address
;    range desired.
; 4. Wait for a time, tnvs (min. 10µs).
; 5. Set the HVEN bit.
; 6. Wait for a time, tpgs (min. 5µs).
; 7. Write data to the FLASH address to be programmed.*
; 8. Wait for a time, tPROG (min. 30µs - max. 40µs)
; 9. Repeat step 7 and 8 until all the bytes within the row are
;    programmed.
;10. Clear the PGM bit.*
;11. Wait for a time, tnvh (min. 5µs).
;12. Clear the HVEN bit.
;13. After time, trcv (min. 1µs), the memory can be accessed in read
;    mode again.
; Osc = 9,8304 MHz Bus CLK = 2.45 MHz -> Cycle = 408 nS
; 5µs -> 13 Cycles
; 10µs -> 25 Cyles
; 30µs -> 74 Cycles
;
;***********************************************************************************
PRGPAGE:        PSHA				;(A) SAVE CONTENTS OF ACCUMULATOR
; 1. Set the PGM bit. This configures the memory for program
;    operation and enables the latching of address and data for
;    programming.
		LDA	#PGM.		        ;SET PGM BIT
		STA	FLCR			;<1> WRITE THIS TO THE FLASH CONTROL REG.
; 2. Read from the FLASH block protect register.
		LDA	FLBPR		        ;<2> READ FROM BLOCK PROT. REG.
; 3. Write any data to any FLASH address within the row address
;    range desired.
		STA	,X			;<3> STORE DATA TO ADDR SPEC. BY H-X (the first byte to program)
; 4. Wait for a time, tnvs (min. 10µs).
		BSR	DEL10
; 5. Set the HVEN bit.
		LDA	#PGM.+HVEN.             ;SET PGM AND HVEN BIT
		STA	FLCR			;<1> WRITE THIS TO THE FLASH CONTROL REG.
; 6. Wait for a time, tpgs (min. 5µs).
		BSR	DEL5
;		
		PSHH				;(B)
		PSHX				;(C) PUSH LO BYTE OF ADDR TO STACK
		CLR	BYTECNT		        ;SET BYTE COUNT TO 0
		CLRX                            ; X = offset into data source
PRPAGELP:
; 7. Write data to the FLASH address to be programmed.*
                CLRH                            ; H:X = Offset into data page
		LDA	DATARAY,X
		PULX				;(C') POP LO BYTE OF ADDR BACK INTO X
		PULH				;(B')
		STA	,X			;<3> STORE DATA TO ADDR SPEC. BY H-X
		AIX	#$$01			;INCREMENT THE ADDRESS
		PSHH				;(B) PUSH LO BYTE OF ADDR BACK TO STACK
		PSHX				;(C)
; 8. Wait for a time, tPROG (min. 30µs).
		BSR	DEL30
; 9. Repeat step 7 and 8 until all the bytes within the row are
;    programmed.
		INC	BYTECNT		        ;INCREMENT THE BYTE COUNTER
		LDX	BYTECNT		        ;LOAD X WITH BYTE COUNT
		CPX	*DATASIZ                ; #64
		BNE	PRPAGELP
		PULX				;C'
		PULH				;B'
;10. Clear the PGM bit.*
		LDA	#HVEN.                  ;SET HVEN BIT (CLEAR PGM)
		STA	FLCR			;<1> WRITE THIS TO THE FLASH CONTROL REG.
;11. Wait for a time, tnvh (min. 5µs).
		BSR	DEL5
;12. Clear the HVEN bit.
		CLRA                            ;CLEAR ALL PROGRAMMING BITS
		STA	FLCR			;<1> WRITE THIS TO THE FLASH CONTROL REG.
;13. After time, trcv (min. 1µs), the memory can be accessed in read
;    mode again.

                PULA				;(A') RESTORE ACCUM
                RTS
;
DEL30:
                BSR     DEL10                   ; 4
                BSR     DEL10                   ; 4
DEL10:
                BSR     DEL5                    ; 4
DEL5:
                NOP
                NOP
                NOP
                NOP
                NOP
                NOP
                RTS                             ; 4 (+ 4 = BSR)
;***********************************************************************************
;; Old Routine
;;PRGPAGE:	PSHA				;(A) SAVE CONTENTS OF ACCUMULATOR
;;		MOV	#PRGTRIES,TEMP2
;;PRGLOOP:	CLR	REPROG
;;		LDA	#PGM.		        ;SET PGM BIT
;;		STA	FLCR			;<1> WRITE THIS TO THE FLASH CONTROL REG.
;;		LDA	FLBPR		        ;<2> READ FROM BLOCK PROT. REG.
;;		PSHH				;(B)
;;		PSHX				;(C) PUSH LO BYTE OF ADDR TO STACK
;;		CLR	BYTECNT		        ;SET BYTE COUNT TO 0
;;		CLRX
;;LDNOTHR:	CLRH
;;		LDA	DATA1,X
;;		PULX				;(C') POP LO BYTE OF ADDR BACK INTO X
;;		PULH				;(B')
;;;	        .if	TESTMOD			;READ INSTEAD OF WRITE DURING TESTING
;;;		LDA	,X			;TO PREVENT ILLEGAL ADDRESS ACCESS
;;;	       .else
;;		STA	,X			;<3> STORE DATA TO ADDR SPEC. BY H-X
;;;	       .endif
;;		AIX	#$$01			;INCREMENT THE ADDRESS
;;		PSHH				;(B) PUSH LO BYTE OF ADDR BACK TO STACK
;;		PSHX				;(C)
;;		INC	BYTECNT		        ;INCREMENT THE BYTE COUNTER
;;		LDX	BYTECNT		        ;LOAD X WITH BYTE COUNT
;;		CPX	#$$08
;;		BNE	LDNOTHR
;;		PULX				;C'
;;		PULH				;B'
;;		AIX	#-1
;;		PSHH				;B
;;		PSHX				;C
;;		LDHX	#FLCR		        ;<5> FOLLOWING SETS THE HVEN BIT IN FLCR
;;		LDA	,X
;;		ORA	#HVEN.
;;		STA	,X
;;		PSHX				;(D) PUSH FLCR (LO BYTE) TO STACK
;;		PSHH				;(E) PUSH FLCR (HI BYTE) TO STACK
;;		LDHX	#TPGM
;;		BSR	DELNUS
;;						;FOLLOWING CLEARS THE HVEN BIT
;;		PULH				;(E') POP FLCR (HI BYTE) FROM STACK
;;		PULX				;(D') POP FLCR (LO BYTE) FROM STACK
;;		LDA	,X
;;		EOR	#HVEN.
;;		STA	,X
;;		PSHX				;(D) PUSH FLCR (LO BYTE) TO STACK
;;		PSHH				;(E) PUSH FLCR (HI BYTE) TO STACK
;;		LDHX	#THVTV		        ;DELAY FOR THVTV
;;		BSR	DELNUS
;;						;SET THE MARG BIT
;;		PULH				;(E') POP FLCR (HI BYTE) FROM STACK
;;		PULX				;(D') POP FLCR (LO BYTE) FROM STACK
;;		LDA	,X
;;		ORA	#MARG.
;;		PSHX				;(D) PUSH FLCR (LO BYTE) TO STACK
;;		PSHH				;(E) PUSH FLCR (HI BYTE) TO STACK
;;		LDHX	#TVTP		        ;DELAY FOR TVTP
;;		BSR	DELNUS
;;		BRA	CLRPGM
;;HALFBRA: BRA	PRGLOOP
;;						;CLEAR THE PGM BIT
;;CLRPGM:	PULH				        ;(E') POP FLCR (HI BYTE) FROM STACK
;;		PULX				;(D') POP FLCR (LO BYTE) FROM STACK
;;		LDA	,X
;;		EOR	#PGM.
;;		STA	,X
;;		LDHX	#THVD		        ;DELAY FOR THVD
;;		BSR	DELNUS
;;; NOW READ WHAT'S BEEN PROGRAMMED AND CHECK IT WITH DATA1-DATA8
;;; BYTECNT IS ALREADY SET AT 8 SO LEAVE IT
;;		PULX				;(C') POP PG LST ADDR (LO-B) FRM STACK
;;		PULH				;(B') POP PG LST ADDR (HI-B) FRM STACK
;;		MOV	#DATA1+7,CURRBYT
;;RDNOTHR:	LDA	,X			;NOW READ DATA AND STORE THEM
;;		DECX				;DEC THIS ADDR NOW BEFORE PUSHING IT
;;		PSHX				;(B) PUSH PG ADDR (LO-B) TO STACK
;;		PSHH				;(C)
;;		CLRH
;;		LDX CURRBYT
;;		CMP	,X
;;		PULH				;(C')
;;		PULX				;(B') POP PG ADDR (LO-B) FR STACK
;;		BNE	FAILVER
;;		DEC	CURRBYT
;;		DBNZ	*BYTECNT ,RDNOTHR  ;DECREMENT THE BYTE COUNTER
;;		BRA	PASSVER
;;FAILVER:	MOV	#$$01,REPROG		;STORE A VALUE IN REPROG TO SIGNAL A
;;						;REPROG OF PAGE
;;PASSVER:        INCX
;;		TXA				;PUT ADDR OF 1ST BYTE OF THIS PAGE
;;		AND	#$$F8			;INTO H:X REGARDLESS OF FAIL/PASS
;;		TAX
;;		PSHH				;(B) PUSH PG 1ST ADDR (LO-B) TO STACK
;;		PSHX				;(C)
;;		LDHX	#FLCR		        ;FOLLOWING CLEARS THE MARG BIT IN FLCR
;;		LDA	,X
;;		EOR	#MARG.
;;		LDA	REPROG
;;		BEQ	PASSED
;;PTPA:		DEC	TEMP2
;;		PULX				;(C') POP PG 1ST ADDR (LO-B) FR STACK
;;		PULH				;(B') POP PG 1ST ADDR (HI-B) FR STACK
;;		BNE	HALFBRA		        ;TRY IT AGAIN
;;		BRA	FAILJMP
;;PASSED:         PULX				;(C')
;;		PULH				;(B')
;;FAILJMP:	AIX	#08			;INDEX TO NEXT PAGE
;;XPRGPAG:	PULA				;(A') RESTORE ACCUM
;;ENDRAMP:	RTS


