/*
 *      registerwin.cpp
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "registerwin.h"

void registerWin::refresh () {
  unsigned char a = dbg->getA (), ccr = dbg->getCcr ();
  unsigned short hx = dbg->getHx (), sp = dbg->getSp ();
  werase (win);
  mvwprintw (win, 0,0,"Accumulatore........... : ");
  if (a != oldA) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,"%02x (%d)", a & 0xff, a);
  if (a != oldA) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }
  oldA = a;
  mvwprintw (win, 1,0,"H:X.................... : ");
  if (hx != oldHx) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,"%04x", hx & 0xffff);
  if (hx != oldHx) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }
  hx = dbg->getHx ();
  mvwprintw (win, 2,0,"Stack Pointer.......... : ");
  if (sp != oldSp) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,"%04x", sp & 0xffff);
  if (sp != oldSp) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }
  oldSp = sp;
  mvwprintw (win, 3,0,"Program Counter........ : %04x", dbg->getPc () & 0xffff);
  mvwprintw (win, 4,0,"Condition Code Register : ");

  if ((ccr ^ oldCcr) & 0x80) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,(ccr & 0x80) ? "V" : "v");
  if ((ccr ^ oldCcr) & 0x80) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }

  if ((ccr ^ oldCcr) & 0x40) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,(ccr & 0x40) ? "1" : "0");
  if ((ccr ^ oldCcr) & 0x40) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }

  if ((ccr ^ oldCcr) & 0x20) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,(ccr & 0x20) ? "1" : "0");
  if ((ccr ^ oldCcr) & 0x20) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }

  if ((ccr ^ oldCcr) & 0x10) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,(ccr & 0x10) ? "H" : "h");
  if ((ccr ^ oldCcr) & 0x10) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }

  if ((ccr ^ oldCcr) & 0x08) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,(ccr & 0x08) ? "I" : "i");
  if ((ccr ^ oldCcr) & 0x08) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }

  if ((ccr ^ oldCcr) & 0x04) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,(ccr & 0x04) ? "N" : "n");
  if ((ccr ^ oldCcr) & 0x04) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }

  if ((ccr ^ oldCcr) & 0x02) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,(ccr & 0x02) ? "Z" : "x");
  if ((ccr ^ oldCcr) & 0x02) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }

  if ((ccr ^ oldCcr) & 0x01) {
    wcolor_set(win, COLOR_RED, NULL);
  }
  wprintw (win,(ccr & 0x01) ? "C" : "c");
  if ((ccr ^ oldCcr) & 0x01) {
    wcolor_set(win, COLOR_NORMAL, NULL);
  }

  oldCcr = ccr;

  wrefresh (win);
}
