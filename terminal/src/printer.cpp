/* Printer.cpp
 * Plain printf implementation of the printer class
 */
 
#include <stdio.h>
#include <stdarg.h>
#include "printer.h"

void Printer::printf (const char *fmt, ...)
{
va_list ap;

	va_start(ap, fmt);
    vprintf (fmt, ap);
    va_end(ap);
}
