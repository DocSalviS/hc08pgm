/* Printer.h
 * Encapsulates printf funcion.
 * Implemented on different platforms, can log the messages
 */

#ifndef PRINTER_H
#define PRINTER_H

class Printer {
#ifdef __WXWINDOWS__
    int m_PageIndex;
#endif
public:
#ifdef __WXWINDOWS__
	void setPageIndex (int p) { m_PageIndex = p; };
#endif
	void printf (const char *fmt, ...);
};

#endif
