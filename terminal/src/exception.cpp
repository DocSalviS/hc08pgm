/* exception.cpp
 */
 
#include <stdio.h>
#include <stdarg.h>
 
#include "exception.h"

Exception::Exception (int perr, const char *fmt, ...)
{
va_list ap;
int len;
char tmp [2];
	err = perr;
	va_start (ap,fmt);
	len = vsnprintf (tmp, 1,fmt, ap);
	va_end (ap);
	message = new char [len + 1];
	va_start (ap,fmt);
	len = vsprintf (message, fmt, ap);
	va_end (ap);
}

Exception::Exception (const char *fmt, ...)
{
va_list ap;
int len;
char tmp [2];
	err = 0;
	va_start (ap,fmt);
	len = vsnprintf (tmp, 1,fmt, ap);
	va_end (ap);
	message = new char [len + 1];
	va_start (ap,fmt);
	len = vsprintf (message, fmt, ap);
	va_end (ap);
}

void Exception::reCode (const char *fmt, ...)
{
va_list ap;
int len;
char tmp [2];
	if (message) {
		delete message;
	}
	va_start (ap,fmt);
	len = vsnprintf (tmp, 1,fmt, ap);
	va_end (ap);
	message = new char [len + 1];
	va_start (ap,fmt);
	len = vsprintf (message, fmt, ap);
	va_end (ap);
}


const char *Exception::error () 
{
	return (err) ? strerror (err) : "";
}
