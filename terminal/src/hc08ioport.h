/*
 * ioport.h
 *
 * Copyright (C) 2016 - salvi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef HC08IOPORT_H
#define HC08IOPORT_H

class IoPort {
  private:
    IoPort *next;
	char *titolo;
  protected:
	unsigned int address;
	unsigned char value;
  public:
	IoPort (unsigned int addr) { address = addr; next = (IoPort*)0; value = 0xff; };
	virtual bool isPort(unsigned int rAddress) { return rAddress==address; };
	virtual unsigned char getValue(unsigned int rAddress) { return value; };
	virtual unsigned char getRowValue(unsigned int rAddress) { return value; };
	virtual void setValue(unsigned int rAddress, unsigned char dato) { value = dato; };
	void resetValue(unsigned char v) { value=v; };
	void setNext (IoPort *n) { next = n; };
	IoPort *getNext () { return next; };
};

#endif