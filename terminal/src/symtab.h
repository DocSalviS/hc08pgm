/* symtab.h */

#ifndef SYMTAB_H
#define SYMTAB_H

#include "symnode.h"
#include "symidx.h"
#include "symfunidx.h"

class SymTab {
	SymNode *symbols;
	SymIdx lineIdx;
	SymIdx addrIdx;
	SymIdx symIdx;
	SymFunIdx *functions;
	int	cSyms;
	int asmSyms;
	
	SymNode *getFunction (const char *file, int line);
  public:
	SymTab () : lineIdx(LINEIDX), addrIdx(ADDRIDX), symIdx(SYMIDX) { symbols=NULL; functions=NULL; cSyms = asmSyms = 0; };
	~SymTab () { clean (); };
	void loadSymTab (const char *source);
	void clean ();
	SymNode *getSymFromAddr (int addr, symType t);
	SymNode *getSymFromLine (const char *file, int line);
	SymNode *getSymFromSym (const char *name, const char *file, int line);
	int hasCSym () { return cSyms; };
	int hasAsmSym () { return asmSyms; };
};

#endif
