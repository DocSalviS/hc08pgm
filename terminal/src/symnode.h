/* symnode.h */

#ifndef SYMNODE_H
#define SYMNODE_H

typedef enum { GET_FIRST, CLINE, ASMLINE, GLOBAL, STATIC, LOCAL } symType;
typedef enum { LINEIDX, ADDRIDX, SYMIDX, SYMSEARCHMODE } idxs;
typedef enum { NONE, ARRAY, FUNCTION, POINER, CODEPOINTER, XDPOINTER, DPOINTER, PPOINER, IPOINTER, LONG, 
	INT, SHORT, CHAR, VOID, FLOAT, STRUCT, BIT, BITFIELD } varTypes;
typedef enum { UNDEFINED, XSTACK, STACK, CODE, SCODE, LORAM, XRAM, RAM, BITSPACE, SFRSPACE, SBITSPACE, REGISTER } addrSpaces;

class SymNode {
	SymNode *next;
	symType type;
	char *fileName;
	char *funName;
	char *symName;
	int level;
	int block;
	int line;
	int address;
	int endAddress;
	
	int varSize;
	addrSpaces addrSpace;
	varTypes varType;
	bool Signed;
	varTypes secVarType;
	varTypes thirdVarType;
	int numEl;
	char *recType;
	bool onStack;
	int stkOffs;
	
	varTypes getVarType (char *source, int &position);
	char *getName (char *source, char term, int &position);
	int getNumber (char *source, char term, int &position, int base = 0);
  public:
  	SymNode (SymNode *n, char *record);
  	SymNode ();
  	~SymNode ();
  	int compare (idxs idx, SymNode *to);
	int hasIdx (idxs idx);
	
#ifdef TREEDEBUG
	void debPrint ();
#endif
	
	void merge (SymNode *tmpSymbol);
	
	void setAddress (int ad) { address = ad; };
	void setType (symType t) { type = t; };
	void setFile (char *sym) { fileName = sym; };
	void setFileAndFun (char *file, char *fun) { fileName = file; funName = fun;};
    void setLine (int ln) { line = ln; };
    void setBlockAndLevel (SymNode *t) { level = (t -> level > 1) ? t -> level : 1; block = (t -> block > 1) ? t -> block : 1; };
	void setSym (char *sym) { symName = sym; };
	char *getFile () { return fileName; };
	int getLine () { return line; };
	int getAddress () { return address; };
	char *getSym () { return symName; };
	symType getType () { return type; };
	SymNode *getNext () { return next; };
	int isFile (const char *file);
	
	int getSize () { return varSize; };
	addrSpaces getAddrSpace () { return addrSpace; };
	varTypes getVarType () { return varType; };
	varTypes getSecVarType () { return secVarType; };
	varTypes getThirdVarType () { return thirdVarType; };
	bool getSigned () { return Signed; };
	int getNumEl () { return numEl; };
	bool getSign () { return Signed; }
	const char *getRecType () { return recType;};
	bool getOnStack () { return onStack; };
	int getStkOffs () { return stkOffs; };
	int getStartAddress () { return address;};
	int getEndAddress () { return endAddress;};
};

#endif
