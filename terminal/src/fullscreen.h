/*
 *      fullscreen.h
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef FULLSCREEN_H
#define FULLSCREEN_H

#ifndef CURSES_H
#define CURSES_H
#include <curses.h>
#endif

#ifndef HCIWINDOW_H
class HcWindow;
#endif

#ifndef IOWIN_H
class IoWin;
#endif

#include "debug.h"

class FullScreen {
    Debug *dbg;
    HcWindow *code;
    HcWindow *data;
    HcWindow *registers;
    HcWindow *command;
    IoWin *io;

    void fulrefresh ();

    void step ();
    void reset ();
    void displayRefresh();

  public:
    FullScreen (Debug *dbg) { this->dbg = dbg; };
    void run (const char *hexFile);
    void disassembly (const char *hexFile, int start, int end);
};

#endif
