/*
 *      iowin.h
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef IOWIN_H
#define IOWIN_H

#ifndef HCIWINDOW_H
#include "hcwindow.h"
#endif

#include "hc08gpioport.h"

#ifndef DISPLAYLCD_H
class DisplayLCD;
#endif

#define S5PIN		0x80
#define YELLOWLED   0x10
#define GREENLED	0x40
#define BUSLEDS		0x0f

class PTALEDout: public OutputConnector {
  public:
	PTALEDout(): OutputConnector(GREENLED | BUSLEDS) {};
	void refresh (WINDOW *win);
};

class PTBLEDout: public OutputConnector {
	InputConnector *in;
	GpIoPort *port;
  public:
	PTBLEDout(InputConnector *myIn, GpIoPort *myPort): OutputConnector(YELLOWLED) { in = myIn; port = myPort; };
	void refresh (WINDOW *win);
};

class IoWin: public HcWindow {
	PTALEDout *ptaLED;
	PTBLEDout *ptbLED;
	InputConnector *ptbKey;
	DisplayLCD *display;
  public:
    IoWin (int x, int y, int w, int h, const char *titolo, Debug *dbg);
    virtual void refresh ();
	void KeyToggle();
};

#endif
