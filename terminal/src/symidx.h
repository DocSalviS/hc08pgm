/* symidx.h */

#ifndef SYMIDX_H
#define SYMIDX_H

#include "symnode.h"
#include "symidxnode.h"

class SymIdx {
	SymIdxNode *root;
	idxs keyNum;
  public :
  	SymIdx (idxs whichKey) { keyNum = whichKey; root = (SymIdxNode *) 0; };
  	~SymIdx () { clean (); };
  	void add (SymNode *n);
  	SymNode *findEq (SymNode *ref);
  	SymNode *findLastEq (SymNode *ref);
  	SymNode *findLt (SymNode *ref);
  	void clean () { if (root) { delete root; } root=(SymIdxNode *) 0;};
#ifdef TREEDEBUG
  	void debPrint ();
#endif
};

#endif
