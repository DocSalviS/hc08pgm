/*
 *      hcwindow.cpp
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "hcwindow.h"

HcWindow::HcWindow (int x, int y, int w, int h, const char *titolo, Debug *dbg) {
  this->dbg = dbg;
  this->titolo = titolo;
  this->h = h - 2;
  this->w = w - 2;
  this->x = x + 1;
  this->y = y + 1;
  win = NULL;
  fullRefresh();
}

void HcWindow::refresh () {
  wrefresh (win);
}

void HcWindow::fullRefresh() {
  if (win) {
	delwin (win);
  }
  WINDOW *frameWin = newwin (h+2, w+2, y-1, x-1);
  box (frameWin, 0, 0);
  mvwprintw (frameWin, 0, 1, "[%s]", titolo);
  wrefresh (frameWin);
  delwin (frameWin);
  win = newwin (h, w, y, x);
  werase (win);
  refresh();
  wrefresh (win);
}

int HcWindow::keyManage () {
  return mvwgetch (win,0,0);
}
