/*
 *      registerwin.h
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef REGISTERWIN_H
#define REGISTERWIN_H

#include "hcwindow.h"

class registerWin: public HcWindow {
    unsigned char oldA, oldCcr;
    unsigned short oldHx, oldSp;
  public:
    registerWin (int x, int y, int w, int h, const char *titolo, Debug *dbg) :HcWindow(x, y, w, h, titolo, dbg) { oldA = dbg->getA (); oldCcr = dbg->getCcr (); oldHx = dbg->getHx (); oldSp = dbg->getSp ();refresh (); };
    virtual void refresh ();
};

#endif
