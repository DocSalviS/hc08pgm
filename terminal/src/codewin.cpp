/*
 *      codewin.cpp
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
#include "codewin.h"

void codeWin::refresh () {
  int i, j, curr, pc;
  pc = dbg->getPc();
  if (pc < start) {
    start = pc;
  } else {
    if (rowstarts) {
      if (pc > rowstarts [h - 1]) {
        if (pc < rowstarts [h - 1] + (rowstarts [h - 1] - rowstarts [0]) / 3) {
          start = rowstarts [h / 2];
        } else {
          start = pc;
        }
      }
    } else {
      rowstarts = new unsigned int [h];
    }
  }
  curr = start;
  werase (win);
  for (i = 0; i < h; i++) {
    char buffer [80];
    int next = dbg->disassembly (curr, buffer);
    rowstarts [i] = curr;
    if (pc >= curr && pc < next) {
      wcolor_set(win, COLOR_RED, NULL);
    }
    mvwprintw (win, i, 0, "%04X ", curr);
    for (j = 0; j < 4; j++) {
      wprintw (win, (curr + j < next) ? "%02X " : "   ", dbg->read (curr + j) & 0xff);
    }
    wprintw (win, " %s", buffer);
    if (pc >= curr && pc < next) {
      wcolor_set(win, COLOR_NORMAL, NULL);
    }
    curr = next;
  }

  wrefresh (win);
}
