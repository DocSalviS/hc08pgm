/*
 *      hcwindow.h
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef HCWINDOW_H
#define HCWINDOW_H

#ifndef CURSES_H
#define CURSES_H
#include <curses.h>
#endif

#include "debug.h"

#define COLOR_NORMAL	0
#define COLOR_RED 		1
#define COLOR_REDLED	2
#define COLOR_YELLOWLED 3
#define COLOR_GREENLED  4
#define COLOR_DISPLAY   5

class HcWindow {
  protected:
   Debug *dbg;
   WINDOW *win;
   const char *titolo;
   int x, y, h, w;
  public:
    HcWindow (int x, int y, int w, int h, const char *titolo, Debug *dbg);
    virtual int keyManage ();
    virtual void refresh ();
	virtual void fullRefresh();
};

#endif

