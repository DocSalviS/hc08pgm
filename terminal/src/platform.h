/* platform.h
 * Defines the __WXMSW__ macro outside wxWidgets
 * if the source is compiled under mingW
 */
 
#ifndef	__WXMSW__
#ifdef __MINGW32_VERSION
#define __WXMSW__
#endif
#endif
