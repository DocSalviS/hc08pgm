/*
 *      memorywin.h
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#ifndef MEMORYWIN_H
#define MEMORYWIN_H

#include "hcwindow.h"

class memoryWin: public HcWindow {
    unsigned short start;
  public:
    memoryWin (int x, int y, int w, int h, const char *titolo, Debug *dbg) :HcWindow(x, y, w, h, titolo, dbg) { start = 0x40; refresh (); };
    virtual void refresh ();
};

#endif
