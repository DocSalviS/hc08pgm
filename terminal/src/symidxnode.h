/* symidxnode.h */

#ifndef SYMIDXNODE_H
#define SYMIDXNODE_H

#include "symnode.h"

class SymIdxNode;
/* Chiave da inserire nel padre */
class SymIdxInsert {
public:
	SymIdxInsert () { right = (SymIdxNode *) 0; data = (SymNode *) 0; };
	SymNode *data;
	SymIdxNode *right;
};

#define	NODELEN		32
class SymIdxNode {
	SymIdxNode *left [NODELEN + 1];
	SymNode *data [NODELEN];

	SymIdxNode *insert (SymNode *n, SymIdxNode *right, idxs keyNum, SymIdxInsert *split);
  public:
  	SymIdxNode ();
  	~SymIdxNode ();
  	SymIdxNode *add (SymNode *n, idxs keyNum, SymIdxInsert *split);
  	SymNode *findEq (SymNode *ref, idxs keyNum);
  	SymNode *findLastEq (SymNode *ref, idxs keyNum, SymNode *lastEqu);
  	SymNode *findLt (SymNode *ref, idxs keyNum);
#ifdef TREEDEBUG
  	void debPrint ();
#endif
};

#endif
