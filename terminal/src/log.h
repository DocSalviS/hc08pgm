/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * log.h
 * Copyright (C) 2016 salvi <stefano@salvi.mn.it>
 *
 * hc08pgm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * hc08pgm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _LOG_H_
#define _LOG_H_

#include <stdio.h>

#ifndef DEBUG_H
class Debug;
#endif

class Log
{
public:
	static void open(const char* name, Debug* xcpu);
	static void printf(const char* format, ...);

protected:

private:
	static FILE* logfile;
	static Debug* cpu;
};

#endif // _LOG_H_

