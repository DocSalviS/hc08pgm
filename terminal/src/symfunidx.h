/* symfunidx.h */

#ifndef SYMFUNIDX_H
#define SYMFUNIDX_H

#include "symnode.h"

class SymFunIdx {
	private: 
		SymFunIdx *next;
		SymNode *function;
		SymNode *startLine;
		SymNode *endLine;
	public:
		SymFunIdx (SymFunIdx *n, SymNode *f) { next = n; function = f; startLine = (SymNode *)0; endLine = (SymNode *)0; };
		void addLines (SymNode *start, SymNode *end) { startLine = start; endLine = end; };
		SymFunIdx *getNext () { return next; };
		int getStartAd () { return function -> getStartAddress (); };
		int getEndAd () { return function -> getEndAddress (); };
		SymNode *getFunction () { return function; };
		int isFun (const char *file, int line) { if (!startLine || !endLine || !file) return 0; if (startLine->isFile (file)) return 0; 
			return (line >= startLine->getLine () && line <= endLine->getLine ()); };
#ifdef TREEDEBUG
		void debPrint () { printf ("Funzione "); function->debPrint(); printf (" da "); if (startLine) startLine->debPrint(); 
		    else printf (" --- \n"); printf (" a "); if (endLine) endLine->debPrint(); else printf (" --- \n"); };
#endif
};

#endif
