/* debug.h
 */
 
#ifndef _debug_h
#define _debug_h

#include "hc08monitor.h"
#include "processor.h"

class Debug : public Processor {	
public:
	Debug (Printer *p) : Processor (p) {}; 
	void readCip (int start, int end);
	void testChip (void);
	void runPgm (const char *hexfile);
	void stepPgm (const char *hexfile);
};

#endif
