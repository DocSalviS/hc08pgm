/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*-  */
/*
 * log.cpp
 * Copyright (C) 2016 salvi <stefano@salvi.mn.it>
 *
 * hc08pgm is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * hc08pgm is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include "debug.h"
#include "log.h"

FILE* Log::logfile = NULL;
Debug* Log::cpu;


void Log::open(const char* name, Debug* xcpu)
{
	cpu = xcpu;
	/*
	logfile = fopen(name, "w");
	if (!logfile) {
		perror ("Non posso aprire il file di log");
	} else {
		printf ("Logfile '%s' aperto\n", name);
	}
	*/
}

void Log::printf(const char* format, ...)
{
	 if (logfile) {
		va_list ap;
		va_start(ap, format);
		fprintf (logfile, "%04X A=%02X CCR=%02X: ", cpu->getPc(), cpu->getA(), cpu->getCcr());
		vfprintf (logfile, format, ap);
		fprintf (logfile, "\n");
		fflush (logfile);
	}
}



