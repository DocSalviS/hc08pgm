/* symidx.cpp */

#include <stdio.h>
#include "symidx.h"

void SymIdx::add (SymNode *n)
{
SymIdxNode *node;
	if (n -> hasIdx (keyNum)) {								// Solo se lo deve inserire
		if (!root) {
			root = new SymIdxNode ();							// Crea il nodo dell'indice
		}
		node = root -> add (n, keyNum, (SymIdxInsert *) 0);	// L'inserimeto puo' cambiare la root
		if (node) {
			root = node;
		}
	}
}

SymNode *SymIdx::findEq (SymNode *ref)
{
	if (root) {
		return root->findEq (ref, (keyNum == SYMIDX) ? SYMSEARCHMODE : keyNum );
	}
	return (SymNode *) 0;
}

SymNode *SymIdx::findLastEq (SymNode *ref)
{
	if (root) {
		return root->findLastEq (ref, keyNum, NULL);
	}
	return (SymNode *) 0;
}

SymNode *SymIdx::findLt (SymNode *ref)
{
	if (root) {
		return root->findLt (ref, keyNum);
	}
	return (SymNode *) 0;
}

#ifdef TREEDEBUG
void SymIdx::debPrint ()
{
	if (root) {
		root->debPrint ();
	} else {
		printf ("Albero vuoto\n");
	}
}
#endif

