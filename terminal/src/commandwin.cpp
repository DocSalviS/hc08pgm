/*
 *      commandwin.cpp
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
#include "commandwin.h"

void commandWin::refresh () {
  // int firstOp = dbg->getFirstOp();
  // int secondOp = dbg->getSecondOp();

  // mvwprintw (win, 0,0,"firstOp %d SecondOp %d", firstOp, secondOp);
  mvwprintw (win, 0,0,"s - step");
  mvwprintw (win, 1,0,"r - reset");
  mvwprintw (win, 2,0,"t - Commuta tasto");
  mvwprintw (win, 3,0,"f - Ridisegna display");
  mvwprintw (win, 4,0,"q - esci");


  wrefresh (win);
}
