/*
 *      fullscreen.cpp
 *
 *      Copyright 2009 salvi <salvi@asus>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "hcwindow.h"
#include "registerwin.h"
#include "memorywin.h"
#include "codewin.h"
#include "commandwin.h"
#include "iowin.h"
#include "fullscreen.h"
#include "log.h"

#define COLOR_HILIGTH   COLOR_BLACK
#define COLOR_BACKGROUND  COLOR_YELLOW

void FullScreen::fulrefresh () {
  code->refresh ();
  registers->refresh ();
  data->refresh ();
  command->refresh ();
  io->refresh();
}

void FullScreen::step () {
  dbg->emulate();
  fulrefresh ();
}

void FullScreen::reset () {
  dbg->reset();
  fulrefresh ();
}

void FullScreen::displayRefresh() {
  code->fullRefresh ();
  registers->fullRefresh ();
  data->fullRefresh ();
  command->fullRefresh ();
  io->fullRefresh();
}

void FullScreen::run (const char *hexFile) {
  printf ("Inizio sessione interattiva su file %s\n", hexFile);

  dbg->emulateProgram (hexFile);

  WINDOW *screen = initscr ();
  start_color();
  noecho();
  init_pair(COLOR_RED, COLOR_HILIGTH, COLOR_BACKGROUND);
  init_pair(COLOR_REDLED, COLOR_RED, COLOR_WHITE);
  init_pair(COLOR_YELLOWLED, COLOR_YELLOW, COLOR_BLACK);
  init_pair(COLOR_GREENLED, COLOR_GREEN, COLOR_WHITE);
  init_pair(COLOR_DISPLAY, COLOR_BLACK, COLOR_WHITE);

  code = new codeWin (0,0,40,18, "Codice", dbg);
  registers = new registerWin (40,0,40,7, "Registri", dbg);
  data = new memoryWin (40,7,40,11, "Dati", dbg);
  // command = new commandWin (0,18,80,6, "Comandi", dbg);
  command = new commandWin (0,18,40,6, "Comandi", dbg);
  io = new IoWin (40,18,40,6, "Input/Output", dbg);

  Log::open("hc08pgm.log", dbg);
  Log::printf("Inizio esecuzione di '%s'", hexFile);

  while (true) {
    switch (command -> keyManage ()) {
      case 's' :  // Step
        step ();
        break;
      case 'r' :  // Reset
        reset ();
        break;
    case 't' :  // Commuta tasto
    io->KeyToggle();
    break;
    case 'f' :  // Rinfresca il contenuto del video
    displayRefresh();
    break;
      case 'q' :  // Quit
        endwin ();
        return;
    }
  }
}
