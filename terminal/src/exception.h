/* exception.h
 */
 
#include <string.h>
 
#ifndef _exception_h
#define _exception_h

class Exception {
public:
	int err;
	char *message;
	
	Exception (int err, const char *fmt, ...);
	Exception (const char *fmt, ...);
	void reCode (const char *fmt, ...);
	~Exception () { if (message) { delete message; } };
	const char *error ();
};

#endif
